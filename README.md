# Der Gelbe Soldat

Corresponding Geocache: [GC5R1DD](https://www.geocaching.com/geocache/GC5R1DD_die-jagd-nach-dem-gelben-soldaten?guid=157336f7-db32-4a05-a33d-81df0decb586)

Corresponding Cartridge (wherigo.com): [Die Jagd nach dem gelben Soldaten](http://www.wherigo.com/cartridge/details.aspx?CGUID=3725d684-37da-427d-9714-a85e40e17c8a)

Corresponding Cartridge (Wherigo Foundation): [WGLF](https://www.wherigofoundation.com/cartridge/cartridge_details.aspx?wg=WGLF)

This project is developed with [Urwigo](https://urwigo.cz).

The media files are in the same folder because for Urwigo is this the best case.