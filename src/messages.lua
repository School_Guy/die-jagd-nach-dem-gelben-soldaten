messages = {}
    messages ["de"] = {}
		--Sperrzone Stage 03
        messages ["de"] [1] = "Hier bist du falsch! Gehe wieder zurueck! Du sollst auf dem Friedrich-Alexander-Platz bleiben!"
        messages ["de"] [2] = "Sorry, falsche Richtung! Bitte gehe die Strasse runter Richtung Sueden!"
		messages ["de"] [3] = [[Willkommen bei der Jagd nach dem gelben Soldaten! Um Verwirrung zu vermeiden:

Wenn ihr keine Zone habt oder nicht weiter weiter wisst, schaut doch einfach mal in eure Tasks. Dort sollte immer stehen, was ihr zu tun habt!

Bei Eingaben achtet bitte darauf, dass ihr immer statt den Umlauten ae,oe oder ue schreibt, das "scharfe S" ist immer ss. Vermeidet bitte, nach den Woertern, die ihr eingebt, Leerzeichen zu machen. Bei der Autokorrektur des Handys werden diese automatisch hinzugefuegt!]]
		messages ["de"] [4] = "Damit wir sicher sein koennen, dass du nicht einer von Kommandant Brauns Komplizen bist, brauchen wir deine Farb-Spuernasen-Agenten-Nummer! Du findest sie auf einem rot-weissen Schild, das wir bisher vor den Farbsoldaten verstecken konnten. Das Schild ist so auffallend unauffaellig zwischen den anderen platziert, dass du als Super-Farb-Spuernase es sicher finden wirst. Bilde aus den Ziffern dieser Tafel eine vierstellige Zahl ohne Komma, damit wir wissen, dass DU es bist!"
		messages ["de"] [5] = "Fortschritt: "
		messages ["de"] [6] = "%"
		messages ["de"] [7] = "1. befreite Farbe"
		messages ["de"] [8] = "2. befreite Farbe"
		messages ["de"] [9] = "3. befreite Farbe"
		messages ["de"] [10] = "4. befreite Farbe"
		messages ["de"] [11] = [[Folge den Soldaten 1/4

Laufe nicht zu schnell in die Richtung, in die die Soldaten laufen/blicken, bis das naechste Bild erscheint!]]
		messages ["de"] [12] = [[Folge den Soldaten 2/4

Laufe nicht zu schnell in die Richtung, in die die Soldaten laufen/blicken, bis das naechste Bild erscheint!]]
		messages ["de"] [13] = [[Folge den Soldaten 3/4

Das hier ist eine Detailaufnahme, diese ist am Boden gemacht worden! Ihr muesst also ganz genau aufpassen wo ihr hin sollt.

Laufe nicht zu schnell in die Richtung, in die die Soldaten laufen/blicken, bis das naechste Bild erscheint!]]
		messages ["de"] [14] = [[Folge den Soldaten 4/4

Das hier ist eine Detailaufnahme, diese ist am Boden gemacht worden! Ihr muesst also ganz genau aufpassen wo ihr hin sollt.

Laufe nicht zu schnell in die Richtung, in die die Soldaten laufen/blicken, bis die naechste Zone erscheint!]]
		messages ["de"] [15] = [[Stadtmotto "Offen aus Tradition":
Die Stadt Erlangen ist schon immer durch Zuzuege gepragt und hat bis heute eine multikulturelle Bevoelkerung. Die Hugenotten pragten die Bevoelkerung einst so sehr, wie heute die grossen internationalen Konzerne und die Universitat. So hat die Stadtverwaltung das Stadtmotto "Erlangen – offen aus Tradition" ausgegeben. Dieses Motto verweist nicht nur auf die historischen Zuwanderungsbewegungen, sondern verweist auch auf den Anspruch, gegenwartig „offen“ zu sein fuer Fremde. Sowohl in der Geschichte als auch in der Gegenwart wurde man diesem Anspruch nicht immer gerecht. Aber nicht zuletzt aufgrund der Wirtschaftskraft der jeweiligen MigrantInnen (frueher wurde die Strumpf-Herstellung mit den Hugenotten nach Erlangen importiert, heute das Wissen von internationalen  IngenieurInnen, arztInnen oder WissenschaftlerInnen) ist es der Bevoelkerung immer wieder gelungen, neben ihren Vorbehalten eine gewisse Sympathie fuereinander zu entwickeln. Das Stadtmotto wurde in einem Logo umgesetzt, das aus 24 kleinen Quadraten besteht, die gemeinsam ein grosses Quadrat mit einer "unbesetzten" oder "offenen" Stelle bilden.]]
		messages ["de"] [16] = [[2. Buecherverbrennung am Schlossplatz 1933:
Zwei Tage nach der ersten Buecherverbrennung in Berlin und vielen anderen Stadten (u.a. Nuernberg) brennen am 12.Mai 1933 auch in Erlangen Buecher, die als "zersetzend fuer den Deutschen Geist" eingeordnet wurden. Der 29-jahrige Bibliothekar Wolfgang Herrmann, Leiter der Zentralstelle fuer das deutsche Bibliothekswesen in Berlin, hatte diese Liste mit Schriften erstellt, die er als schadigend fuer das deutsche Volk ansah. Die von Herrmann erstellten „Schwarzen Listen“ wurden fortlaufend erganzt und erweitert. Auf der „Liste des schadlichen und unerwuenschten Schrifttums“, die ab 1935 regelmassig herausgegeben wurde, fanden sich schliesslich 12.400 Titel und das Gesamtwerk von 149 Autoren.
Jeder Student hatte zuerst einmal seine eigene Buecherei und auch die seiner Bekannten von „schadlichen“ Buechern zu saubern, danach wurden die Universitats- und Institutsbibliotheken durchforstet. Auch oeffentliche Bibliotheken und Buchhandlungen wurden nach "verbrennungswuerdiger" Literatur durchsucht oder wurden dazu angehalten, ihre Bestande selbst zu "saubern" und die Buecher freiwillig zu uebergeben. Unterstuetzung erhielten die Studenten von ihren Professoren und Rektoren, die nicht nur spater bei den Verbrennungsfeiern erschienen, sondern auch in den Kampfausschuessen zur Aussonderung des zum Verbrennen bestimmten Materials mitarbeiteten.]]
		messages ["de"] [17] = [[Erich Kastner:
Der Schriftsteller Erich Kastner (* 23. Februar 1899 in Dresden; + 29. Juli 1974 in Muenchen) zahlte zu den Autoren, deren Gesamtwerk seit 1933 auf der Schwarzen Liste zu finden war. Die Begruendung: Seine "kulturbolschewistischen Haltung im Schrifttum vor 1933". Der Buecherverbrennung sah Kastner von nachster Nahe aus zu.

Warum er Sozialist, Pazifist und Regimekritiker wurde, beschreibt er rueckblickend auf den ersten Weltkrieg folgendermassen: "Das entscheidende Erlebnis war natuerlich meine Beschaftigung als Kriegsteilnehmer. Wenn man 17-jahrig eingezogen wird, und die halbe Klasse ist schon tot, weil bekanntlich immer zwei Jahrgange ungefahr in einer Klasse sich ueberlappen, ist man  noch weniger Militarist als je vorher [...]".
Er war einer der wenigen Regimegegner, die auch nach der Machtergreifung Hitlers in Deutschland als Schriftsteller arbeiteten. Es war ihm ein Anliegen, als "Chronist der Ereignisse" vor Ort zu sein. Kastners Wohnung in Charlottenburg wurde 1944 durch Bomben zerstoert. Anfang 1945 gelang es ihm, mit einem Filmteam zu angeblichen Dreharbeiten nach Tirol zu reisen und dort das Kriegsende abzuwarten.

Von Kastners Gesamtwerk war das Kinderbuch "Emil und die Detektive" (Oktober 1929) zunachst explizit von der Buecherverbrennung ausgenommen. Erst im Jahr 1936 wurde es nachtraglich in die stark angewachsenen Listen "undeutscher Literatur" aufgenommen und von da an ebenfalls vom Markt genommen und verbrannt.]]
		messages ["de"] [18] = [[4. Palais Stutterheim: Zusammentreiben der Juden / Deportation:
Amtshauptmann Christian Hieronymus Freiherr von Stutterheim begann 1728 mit dem Bau des Palais, das durch seine schlichte Schoenheit des stattlichen Baus besticht. Nach mehreren Besitzerwechseln ging das Palais im Jahr 1836 an die Stadt Erlangen ueber, das Haus wurde zum Rathaus und beherbergte u.a. das Wachzimmer der Landwehr und die Kreissparkasse.

Bei dem in Erlangen als "Judenaktion" bezeichneten Pogrom in der Nacht vom 9. auf den 10. November 1938 spielte das Gebaude als Rathaus und Amtssitz der Polizei eine beschamende Rolle. Wahrend sich die SA auf dem Marktplatz sammelte, wurden auch die dienstfreien Beamten der Schutz- und Kriminalpolizei zur Wache gerufen. Im Rathaus organisierten der Oberbuergermeister Alfred Gross (1934 - 1944), der Leiter der Polizei Egidius Wolf und der SA-Sturmbannfuehrer Otto Klein mit anderen das Vorgehen. Dann wurden aus je einem Polizeibeamten und zwei SA-Mannern bestehende Trupps gebildet, die ab etwa 2 Uhr die etwa 43 Erlanger Juden in ihren Wohnungen verhafteten und alle – Kinder, Jugendliche, Erwachsene, alte Leute, Manner und Frauen – zum Rathaus brachten. Waehrend ihre Wohnungen teilweise nach Wertpapieren durchsucht wurden, standen die Erlanger Juden zusammen mit drei Leidensgefahrten aus Baiersdorf und sieben aus Forth stundenlang bei Kalte und stroemendem Regen im Hof des Rathauses (also des Palais Stutterheim), nur Alte und Gebrechliche sowie die Kinder durften in den Zellen bleiben.
Gegen Mittag schaffte man die juedischen Frauen und Kinder fuer mehrere Tage in das stadtische Obdachlosenasyl in der Woehrmuehle. Die Manner kamen ins Amtsgerichtsgefangnis in der Adolf-Hitler-Str. 14, von dort wurden sie am nachsten Tag ins Gefangnis nach Nuernberg ueberstellt.]]
		messages ["de"] [19] = [[5. Hugenottenkirche / Zuzug der Hugenotten:
Die ersten Zuwanderer haben das Stadtbild entscheidend gepragt: Die Gruendung der "Hugenottenstadt" im Jahr 1686 erkennt man noch heute in den schachbrettartig angelegten Hauserzeilen der Neustadt. Zirka 1500 Hugenotten (oder auch Calivinisten) kamen in dem Jahr nach Erlang(en). Markgraf Christian Ernst bot den Fluechtlingen das Recht auf Ansiedlung in seinem noch an den Folgen des Dreissigjahrigen Krieges leidenden Fuerstentum an, um dessen Wirtschaft durch die Ansiedlung moderner Gewerbe zu foerdern. Er war damit einer der ersten lutherischen Fuersten in Deutschland, der Calvinisten in seinem Land aufnahm und ihnen sogar freie Religionsausuebung zusicherte. Noch bevor abzusehen war, mit wievielen Fluechtlingen gerechnet werden konnte, beschloss der Markgraf, suedlich der Altstadt eine eigenstandige Siedlung (die "Neustadt Erlangen") zu gruenden. Mit dem rationalen Motiv, die Wirtschaft des eigenen Landes zu foerdern, verband sich in fuer den Absolutismus typischer Weise die Hoffnung auf den Ruhm als Stadtgruender.
Die Neustadt wurde nicht nur zum Ziel der Hugenotten, sondern auch von Lutheranern und Deutsch-Reformierten, denen jeweils dieselben Privilegien wie den Hugenotten verliehen worden waren. 1698 lebten 1000 Hugenotten und 317 Deutsche in Erlangen. Aufgrund der weiteren Zuwanderung wurden die Hugenotten jedoch bald zu einer franzoesisch sprechenden Minderheit in einer deutschen Stadt. Der franzoesische Einfluss nahm in der Folgezeit weiter ab. So wurde 1822 zum letzten Mal ein Gottesdienst in der Hugenottenkirche in franzoesischer Sprache gehalten.]]
		messages ["de"] [20] = [[6. Pinnadeln in Erlangen:
Die Erlanger Kuenstlerin Isi Kunath hat im Jahr 2002 insgesamt 16 verschwundene historische Orte und Denkmale im ganzen Stadtgebiet mit grossen rotkoepfigen Pin-Nadeln zu markiert. Das Kunstwerk zur 1000-Jahr-Feier wurde am Donnerstag, 11. April 2002, auf dem Hugenottenplatz vollendet und offiziell uebergeben. Eigentlich sollten die Pin-Nadeln und die dazugehoerigen Info-Tafeln nur fuer ein Jahr aufgestellt werden, aber sie wurden nach anfanglichen Zweifeln in der Bevoelkerung akzeptiert. Im Herbst 2015 werden sie aufgrund der Material-Abnutzung auf Anraten der Kunstkommission der Stadt Erlangen endgueltig abgebaut.]]
		messages ["de"] [21] = [[7. Evangelisch-Lutherische Kirche gestern und heute:
Die Kirchen haben in der deutschen Geschichte leider immer wieder sehr fragwuerdige Positionen vertreten. Auch in Erlangen wurden von einzelnen Theologen nationalistische und judenfeindliche Gedanken unterstuetzt. 1933 entwarf der lutherische Theologe Paul Althaus mit seinem Kollegen Werner Elert das Gutachten der Erlanger Theologischen Fakultat zu einem geplanten Arierparagraphen der Reichskirche. Althaus und Elert fordern in ihrem Gutachten, "nichtarische" Bewerber um ein kirchliches Amt auszuschliessen. Bereits eingestellte "Nichtarier" sollten jedoch – entgegen den Forderungen der Deutschen Christen – nicht aus ihren amtern entlassen werden. Erst 1936 beginnt Althaus, sich von den Nationalsozialisten zu distanzieren.
Heute ist die Kirche vielerorts aktive Gestalterin von Friedensprozessen. Vom Dritte-Welt-Laden, der faire Bezahlung und Arbeitsbedingungen foerdert, bis zum Friedensweg der Religionen in Erlangen, in dem alle Religionsgemeinschaften zusammenarbeiten, um Friedensprozesse zu foerdern. Dieser Friedensweg der Religionen entwickelte auch die Idee einer Stadtfuehrung "Offen aus Tradition in Erlangen?", die zum Ausgangspunkt fuer "Die Jagd nach dem Gelben Soldaten" wurde.
Die Gymnasialpadagogische Materialstelle der Evangelisch-Lutherischen Kirche hat den Ort fuer das Final freigegeben. Wir danken den Mitarbeitenden der Materialstelle, dass wir den boesen Kommandeur Mr.Braun auf ihrem Grundstueck gefangen nehmen duerfen!]]
		messages ["de"] [22] = [[8. Rolle der Universitat im 3.Reich:
Die Universitat(en) spielten im Aufbluehen des Nationalsozialismus eine ausserst ungute Rolle. Offenbar hatte die Idee von Menschen 1.ter und 2.ter Klasse einen fruchtbaren Boden im damals sehr elitaren Denken vieler Professoren und Studenten. Unruehmlich tat sich die Universitat Erlangen hervor: Als erste deutsche Hochschule hatte sie bereits im Jahr 1929 ein mehrheitlich von Nationalsozialisten beherrschtes Studentenparlament. Die Gesinnung der Studierenden zeichnete sich spatestens ab diesem Zeitpunkt durch Antirationalismus, Frontsoldatenmythos, voelkischen Nationalismus, Antisemitismus und Verachtung des Weimarer Parteienstaates aus. Es wurden unter anderem vielen juedischen Menschen ihre Titel aberkannt (Depromotion), noch bedrohlicher aber waren offenbar die Studienausschluesse: Der gemeinsame Nenner bestand darin, dass all diejenigen zu entfernen seien, die sich in "antinationalem Sinne aktiv betatigt" hatten – sei es als Mitglieder oder Sympathisanten kommunistischer, sozialdemokratischer oder sonstiger missliebiger Parteien und Gruppierungen. Und natuerlich die Ausschluesse von "Nichtariern" von Pruefungen und von Vorlesungen, wenn sie nicht gleich komplett aus der Universitat entfernt wurden. Beide Gruppen (Politisch unliebsame und Nichtarier, besonders Juden) wurden zumeist entweder direkt aus der Universitat oder in den Jahren nach ihrem akademischen Ausschluss entweder in Vernichtungslager gebracht oder mussten fliehen.]]
		messages ["de"] [23] = [[9. Steinfiguren auf dem Schloss:
Die Steinfiguren auf dem Erlanger Schloss reprasentieren die zur Bauzeit um 1700 bekannten Kontinente. Darunter ein Mann mit Turban, der fuer Asien steht (heute wuerde man wohl eher den Nahen oder Mittleren Osten mit ihm assoziieren). Ganz positiv ist er dargestellt, zu dieser Zeit sind Handler aus dem Osten beliebt, sie bringen Gewuerze und sogar Kaffee bis ins Heilige Roemische Reich Deutscher Nation.
Spater beim Reiterstandbild wird man sehen, wie ein Mann mit Turban mit dem Schwert abgeschlagen wird. Das liegt zeitlich nah beieinander – doch dazu spater mehr.
Wir hoffen, dass die Figuren nicht in den nachsten Jahren abgebaut werden muessen, wie die Goetter-Statuen auf der Vorderseite des Schlosses, die am 25.6.2015 mit dem Kran heruntergehoben wurden, um sich etwa zwei oder drei Jahre lang in einer Restaurationswerkstatt vom offenbar anstrengenden Treiben auf Erlangens Marktplatz zu erholen.]]
		messages ["de"] [24] = [[10. Rueckert-Brunnen: Was ist das Besondere an Rueckert?
Friedrich Rueckert (* 16. Mai 1788 in Schweinfurt; + 31. Januar 1866 in Neuses (heute Teil von Coburg); Pseudonym Freimund Raimar, Reimar oder Reimer) war ein  deutscher  Dichter, Sprachgelehrter und uebersetzer sowie einer der Begruender der deutschen Orientalistik. Im Jahr 1826 wurde der Literat Professor fuer Orientalistik in Erlangen und lehrte und schrieb dort, bis er 1841 von Koenig Friedrich Wilhelm IV nach Berlin berufen wurde. Er beherrschte mindestens 44 Sprachen... Afghanisch, Albanisch, Altkirchenslawisch, Arabisch, Armenisch, Altathiopisch, Avestisch, Azeri, Berberisch, Biblisch-Aramaisch, Englisch, Estnisch, Finnisch, Franzoesisch, Gotisch, Griechisch, Hawaiisch, Hebraisch, Hindustanisch, Italienisch, Kannada, Koptisch, Kurdisch, Latein, Lettisch, Litauisch, Malaiisch, Malayalam, Maltesisch, Neugriechisch, Persisch, Pali, Portugiesisch, Prakrit, Russisch, Samaritanisch, Sanskrit, Schwedisch, Spanisch, Syrisch, Tamil, Telugu, Tschagataisch, Tuerkisch.
Er uebersetzte unter anderem den Koran, und zwar auf eine ganz besondere Art und Weise: nicht wortwoertlich, sondern lyrisch. So entstanden Texte, in denen der Wohlklang der Koransuren, der literarische und dichterische Reiz, deutlich wurden.]]
		messages ["de"] [25] = [[11. Reiterstandbild:
Das Reiterstandbild zeigt den Markgrafen Christian Ernst, und wurde in den Jahren 1711/12 geschaffen. Zur Fertigung wurde ein Sandsteinblock aus den Steinbruechen am Burgberg gehauen.
Das Denkmal erinnert an die Teilnahme des Markgrafen an den "Tuerckenkriegen" und zeigt ihn auf dem Pferd in voller Ruestung. Unter den Tuerkenkriegen versteht man die Kriege zwischen dem sich nach dem Untergang von Byzanz im Jahre 1453 nach Norden und Westen ausbreitenden Osmanischen Reich und dem christlich gepragten Europa. Die Osmanen waren jedoch bei weitem nicht nur Muslime, sondern auch viele Menschen, die dem orthodoxen Christentum angehoerten. Es war also viel mehr ein Krieg um Vormachtsstellungen und Handelswege, als ein religioes motivierter Krieg.
Unter dem Reiterstandbild von Markgraf Christian Ernst liegen ein abgeschlagener Osmane sowie Invidia, die Personifikation des Neids.]]
		messages ["de"] [26] = [[12. Jakob-Herz-Denkmal:
Jakob Herz (* 2. Februar 1816 in Bayreuth; + 27. September 1871 in Erlangen) war Arzt und spater erster juedischer Professor in Bayern bzw. Erlangen. Seine Verdienste waren weniger im wissenschaftlichen (seine Doktorarbeit hatte Klumpfuesse zum Inhalt) als vielmehr im zwischenmenschlichen Bereich angesiedelt. Aufgrund seiner Beliebtheit als leidenschaftlicher Lehrer und ueberzeugter Arzt haben Freunde und PatientInnen ihm zu Ehren gleich nach seinem Tod eine Statue errichtet; die erste fuer einen Juden in Bayern.
Diese wurde am 6. Mai 1875 in Form einer auf einem Steinsockel stehenden doppeltlebensgrossen Bronzestatue, die Herz im Gehrock darstellte, feierlich enthuellt. Sein Standort war der Holzmarkt, der heutige Hugenottenplatz, wo sich damals eine Schule befand. Am 14. September 1933 beschloss die NS-Stadtratsfraktion in Erlangen unter Vorsitz des Oberbuergermeisters Hans Flierl, "eine Kulturschande, die das ganze deutsche Volk als solche empfinden musste, wieder gutzumachen" , sprich das Herz-Denkmal auf dem damaligen Luitpoldplatz zu entfernen. 1983 folgte die Stele zwischen Krankenhaus- und Universitatsstrasse, die „Ein Denkmal an ein Denkmal" darstellen und an die urspruengliche Statue erinnern sollte.]]
		messages ["de"] [27] = [[Friedensweg der Religionen:
Der Friedensweg der Religionen entwickelte die Idee einer Stadtfuehrung "Erlangen - Offen aus Tradition? Zwischen Wunsch und Wirklichkeit", die zum Ausgangspunkt fuer "Die Jagd nach dem Gelben Soldaten" wurde. Der Friedensweg der Religionen ist ein interreligioeses Projekt in Erlangen, das die Verstandigung zwischen den Religionen starken will. Dazu werden vor allem einmal pro Jahr Friedenswege angeboten mit interreligioesen Gebeten und Gesangen. Am Bohlenplatz hat der Friedensweg eine Stele aufgestellt mit den verschiedenen Symbolen der Religionen: Baha'i, Buddhismus, Christentum, Islam und Judentum.
Das Bild zeigt die Enthuellung der Stele im Herbst 2013, hier mit Altoberbuergermeister Siegfried Balleis und dem Evang.-Luth. Dekan Peter Huschke.
Die Gymnasialpadagogische Materialstelle der Evangelisch-Lutherischen Kirche hat den Ort fuer das Final freigegeben. Wir danken den Mitarbeitenden der Materialstelle, dass wir den boesen Kommandeur Mr.Braun auf ihrem Grundstueck gefangen nehmen duerfen!]]
		messages ["de"] [28] = "Hebel 1 ist in Position: "
		messages ["de"] [29] = [[
Hebel 2 ist in Position: ]]
		messages ["de"] [30] = [[
Hebel 3 ist in Position: ]]
		messages ["de"] [31] = "oben"
		messages ["de"] [32] = "unten"
		messages ["de"] [33] = "mitte"
		messages ["de"] [34] = [[Hebel werden auf "oben - unten - oben" gestellt. Blaue Wasserfontaenen ergiessen sich. Juhu, die dritte Farbe ist befreit.]]
		messages ["de"] [35] = "Von hinten schreit dich eine Figur mit Turban an: Juhu! Danke!! Schau doch mal zu meinem Freund dem Rueckert, ob an seinem Brunnen das Wasser auch laeuft! Das waere toll von dir."
		messages ["de"] [36] = "Was ist das? Auf dem Wasser schwimmt ein Blatt Papier! Eine Sauerei. Du nimmst es und willst es wegschmeissen."
		messages ["de"] [37] = "Da siehst du, dass etwas drauf steht. Hmm. Und jetzt? Lass es erstmal trocknen. Es sollte auch nicht laenger als 10 sec bei dieser Groesse dauern."
		messages ["de"] [38] = [["Klick" - Nichts passiert!]]
		messages ["de"] [39] = "Du hast keine Entfernung festgelegt! Bitte erledige das noch bevor du die moeglich Finalzone festlegst!"
		messages ["de"] [40] = "Hm, der Zettel ist jetzt zwar noch nicht trocken, ich sollte aber schon mal zum Rueckert-Brunnen gehen, um nachzusehen, ob alles in Ordnung ist, wie von den Statuen gewuenscht."
		messages ["de"] [41] = "Prima geloest! Da du diese Aufgabe exzellent bewaeltigt hast, darfst du jetzt deine erste Kontaktperson suchen gehen. Sie wird dich in alles Weitere einfuehren!"
		messages ["de"] [42] = "Stehst du an der Nordseite des Bahnhofgebaeudes?"
		messages ["de"] [43] = "Das rot-weisse Schild ist als Hydranten-Hinweis-Schild getarnt."
		messages ["de"] [44] = "Du stehst aussen an der Ecke zwischen den Taxis und Gleis 1, ja? Da sind drei Schilder an der Wand des Bahnhofs angebracht…"
		messages ["de"] [45] = [[Sind:
]]
		messages ["de"] [46] = [[ m und
]]
		messages ["de"] [47] = "* in Ordnung?"
		messages ["de"] [48] = "Ja"
		messages ["de"] [49] = "Nein"
		messages ["de"] [50] = [[Du: Entschuldigung, haben sie jemanden gesehen, der hier Gelb entfernt?
Frau: Hund Katze Maus Elefant...
Du: ENTSCHULDIGUNG?!
Frau: Giraffe... Nein, Kaninchen ... Ich sollte mir ein Tier merken, ich weiss nur noch, hier ist irgendwo ein Bild. ]]
		messages ["de"] [51] = "Hallo "
		messages ["de"] [52] = [[, schoen dich zu sehen. Ich bin Friedrich Alexander von Brandenburg Bayreuth. Willkommen in unserer schoenen Stadt Erlangen. Kennst du unser Stadtmotto?     Nein?!
Na: "Offen  aus Tradition!". Wir haben hier ganz schoen viel erlebt in Sachen Vielfalt und Offenheit...]]
		messages ["de"] [53] = "Aber im Moment ist etwas merkwuerdig, immer mehr Farben verschwinden, alles wird grau. Ich glaube, dass liegt an diesen bunten Soldaten. Sie scheinen alle Farben gefangen zu nehmen und wegzusperren. So eine triste Zeit gab es hier schon einmal! 1933 gab es auch viele Uniformierte und hier auf dem Schlossplatz wurden sogar Buecher verbrannt."
		messages ["de"] [54] = "Hier in diesem Geschichtsbuch steht ein bisschen darueber. Du darfst es gerne behalten."
		messages ["de"] [55] = [[Es oeffnet sich nun gleich zum ersten Mal eine Seite des Geschichtsbuches. Diese enthaelt Informationen, die nicht relevant sind zum Loesen des Caches, sondern sie dienen nur zur Information! Ihr koennt die Eintraege jederzeit im Item "Geschichtsbuch" abrufen.]]
		messages ["de"] [56] = [[Stadtmotto "Offen aus Tradition":
Seite 1/2:

Die Stadt Erlangen ist schon immer durch Zuzuege gepraegt und hat bis heute eine multikulturelle Bevoelkerung. Die Hugenotten praegten die Bevoelkerung einst so sehr, wie heute die grossen internationalen Konzerne und die Universitaet. So hat die Stadtverwaltung das Stadtmotto "Erlangen - offen aus Tradition" ausgegeben. Dieses Motto verweist nicht nur auf die historischen Zuwanderungsbewegungen, sondern verweist auch auf den Anspruch, gegenwaertig fuer Fremde "offen" zu sein. Sowohl in der Geschichte als auch in der Gegenwart wurde man diesem Anspruch nicht immer gerecht. ]]
		messages ["de"] [57] = [[Seite 2/2:

Aber nicht zuletzt aufgrund der Wirtschaftskraft der jeweiligen MigrantInnen (frueher wurde die Strumpf-Herstellung mit den Hugenotten nach Erlangen importiert, heute das Wissen von internationalen  IngenieurInnen, AerztInnen oder WissenschaftlerInnen) ist es der Bevoelkerung immer wieder gelungen, neben ihren Vorbehalten eine gewisse Sympathie fuereinander zu entwickeln. Das Stadtmotto wurde in einem Logo umgesetzt, das aus 24 kleinen Quadraten besteht, die gemeinsam ein grosses Quadrat mit einer "unbesetzten" oder "offenen" Stelle bilden.]]
		messages ["de"] [58] = [[Ich habe damals eines der Buecher gerettet. Willst du es haben?

Ja, warum nicht!
Nein, ich will das angerusste Buch nicht ...]]
		messages ["de"] [59] = "Moment, was war das ueberhaupt fuer ein Buch? Ah... Erich Kaestner...  Emil und die Detektive. Hoppla, das ist ja ein wichtiger Teil der deutschen Geschichte! Tut mir leid, das wuerde ich doch gerne selber behalten. "
		messages ["de"] [60] = "Eher nicht? Hm, was war das ueberhaupt fuer ein Buch? Ah... Erich Kaestner...  Emil und die Detektive. Hoppla, das ist ja ein wichtiger Teil der deutschen Geschichte! Na, das wuerde ich eh lieber selber behalten."
		messages ["de"] [61] = [[Erich Kaestner:
Seite 1/2:

Der Schriftsteller Erich Kaestner (* 23. Februar 1899 in Dresden; + 29. Juli 1974 in Muenchen) zaehlte zu den Autoren, deren Gesamtwerk seit 1933 auf der Schwarzen Liste zu finden war. Die Begruendung: Seine "kulturbolschewistische Haltung im Schrifttum vor 1933". Der Buecherverbrennung sah Kaestner von naechster Naehe aus zu.
Warum er Sozialist, Pazifist und Regimekritiker wurde, beschreibt er rueckblickend auf den ersten Weltkrieg folgendermassen: "Das entscheidende Erlebnis war natuerlich meine Beschaeftigung als Kriegsteilnehmer. Wenn man 17-jaehrig eingezogen wird, und die halbe Klasse ist schon tot, weil bekanntlich immer zwei Jahrgaenge ungefaehr in einer Klasse sich ueberlappen, ist man noch weniger Militarist als je vorher […]".]]
		messages ["de"] [62] = [[Seite 2/2:

Er war einer der wenigen Regimegegner, die auch nach der Machtergreifung Hitlers in Deutschland als Schriftsteller arbeiteten. Es war ihm ein Anliegen, als "Chronist der Ereignisse" vor Ort zu sein. Kaestners Wohnung in Charlottenburg wurde 1944 durch Bomben zerstoert. Anfang 1945 gelang es ihm, mit einem Filmteam zu angeblichen Dreharbeiten nach Tirol zu reisen und dort das Kriegsende abzuwarten.
Von Kaestners Gesamtwerk war das Kinderbuch „Emil und die Detektive“ (Oktober 1929) zunaechst explizit von der Buecherverbrennung ausgenommen. Erst im Jahr 1936 wurde es nachtraeglich in die stark angewachsenen Listen "undeutscher Literatur" aufgenommen und von da an ebenfalls vom Markt genommen und verbrannt.]]
		messages ["de"] [63] = "Wenn ich dir schon dieses Buch nicht geben mag, weiss ich wenigstens wo du vielleicht andere Buecher findest. Gehe hierfuer in Richtung 205 Grad auf diesem Platz, dein Geraet meldet sich dann wieder, wenn du angekommen bist!"
		messages ["de"] [64] = "Finde die naechste Zone! (205* ab dem Denkmal)"
		messages ["de"] [65] = "Finde die naechste Zone!"
		messages ["de"] [66] = [[Willkommen in der Stadtbibliothek.
Wie lautet die Nummer deines Leseausweises?]]
		messages ["de"] [67] = "Hier an der Kirche ist Gemeindefest. Er muss im Getuemmel untergetaucht sein. Vielleicht weiss der Geistliche in der Kirche mehr."
		messages ["de"] [68] = "Das Gemeindefest ist  ganz schoen bunt. Vielleicht, weil die Leute vom Dritte-Welt-Laden der Gemeinde mitmachen. Die haben ja auch ganz schoen viel Erfahrung mit Offenheit und kennen sich aus mit dem Kampf gegen Mister Braun."
		messages ["de"] [69] = "Schau an! Da sind rote Luftballons! Rot! In all dem Grau hier! Die nehm ich mit."
		messages ["de"] [70] = "Die Bibliothek hat noch offen. Du gehst hinein und reichst der Bibliothekarin das gelbe Buch."
		messages ["de"] [71] = [[Bibliothekarin: Wow, du hast ein farbiges Buch und wuerdest es mir geben? Seit Wochen werden die Farben hier immer weniger, alles ist schon grau. Einzig dieses gruene Kaestchen hier hat noch Farbe. Dafuer hat keiner den Schluessel und niemand weiss, was drin ist.
Du scheinst ja Experte fuer seltene Gegenstaende mit Farbe zu sein, moechtest du das Kaestchen vielleicht mitnehmen?]]
		messages ["de"] [72] = "Bibliothekarin: Viel Spass damit. Vielleicht bekommst du es auf und kannst Farbe in unsere Welt zurueck bringen."
		messages ["de"] [73] = [[Bibliothekarin: Was bist denn du fuer ein Cacher? Du hast den Auftrag die Welt zu retten und willst nicht mal ein virtuelles Kaestchen durch die Gegend tragen?
Ich geb dir das jetzt einfach mit. Basta! Du brauchst es um die Welt zu retten, da dulde ich keine Widerrede.
Dass hier einer programmiert hat, dass man nein sagen koennte, ist ja schon schlimm genug, aber dass Typen wie du das auch noch waehlen schlaegt dem Farbfass den Boden aus. Unglaublich, unglaublich...]]
		messages ["de"] [74] = "Du hoerst ein Gespraech. Du siehst dich um. Aber da ist niemand. Du siehst nach oben und dir wird klar, es sind die Steinfiguren, die sich unterhalten."
		messages ["de"] [75] = [["Ich bin dieses langweilige Dasein hier oben so leid. Rumstehen und Kontinente repraesentieren... Super!" ]]
		messages ["de"] [76] = [["Aber echt. Womit haben wir das eigentlich verdient?" ]]
		messages ["de"] [77] = [["Die Aussicht ist auch Mist. Jetzt erst recht, seit die ganzen Farben verschwunden sind."]]
		messages ["de"] [78] = [["Da hast du Recht. Grau sind wir selber schon."]]
		messages ["de"] [79] = [["Wird Zeit, dass wieder Farbe in unser Leben kommt."]]
		messages ["de"] [80] = [["Oh schaut mal: Da unten steht einer. Und er zieht eine Spur gruener Farbe hinter sich her." ]]
		messages ["de"] [81] = [["Wenn das kein Lichtblick ist. Vielleicht kann er den Brunnen auch wieder zum Laufen bringen, dann waere das hier wieder viel angenehmer!"]]
		messages ["de"] [82] = [[9. Steinfiguren auf dem Schloss:

Seite 1/2:
Die Steinfiguren auf dem Erlanger Schloss repraesentieren die zur Bauzeit um 1700 bekannten Kontinente. Darunter ein Mann mit Turban, der fuer Asien steht (heute wuerde man wohl eher den Nahen oder Mittleren Osten mit ihm assoziieren). Ganz positiv ist er dargestellt, zu dieser Zeit sind Haendler aus dem Osten beliebt, sie bringen Gewuerze und sogar Kaffee bis ins Heilige Roemische Reich Deutscher Nation.]]
		messages ["de"] [83] = [[Seite 2/2:
Spaeter beim Reiterstandbild wird man sehen, wie ein Mann mit Turban mit dem Schwert abgeschlagen wird. Das liegt zeitlich nah beieinander - doch dazu spaeter mehr.
Wir hoffen, dass die Figuren nicht in den naechsten Jahren abgebaut werden muessen, wie die Goetter-Statuen auf der Vorderseite des Schlosses, die am 25.6.2015 mit dem Kran heruntergehoben wurden, um sich etwa zwei oder drei Jahre lang in einer Restaurationswerkstatt vom offenbar anstrengenden Treiben auf Erlangens Marktplatz zu erholen.]]
		messages ["de"] [84] = "Schlossgarten (Brunnen)"
		messages ["de"] [85] = "Das Wasser sprudelt! Toll gemacht. Aber Rueckert ist nicht da. Was tust du jetzt?"
		messages ["de"] [86] = [[10. Rueckert-Brunnen: Was ist das Besondere an Rueckert?

Seite 1/2:
Friedrich Rueckert (* 16. Mai 1788 in Schweinfurt; + 31. Januar 1866 in Neuses (heute Teil von Coburg); Pseudonym Freimund Raimar, Reimar oder Reimer) war ein deutscher Dichter, Sprachgelehrter und Uebersetzer sowie einer der Begruender der deutschen Orientalistik. Im Jahr 1826 wurde der Literat Professor fuer Orientalistik in Erlangen und lehrte und schrieb dort, bis er 1841 von Koenig Friedrich Wilhelm IV nach Berlin berufen wurde. ]]
		messages ["de"] [87] = [[Seite 2/2:
Er beherrschte mindestens 44 Sprachen... Afghanisch, Albanisch, Altkirchenslawisch, Arabisch, Armenisch, Altathiopisch, Avestisch, Azeri, Berberisch, Biblisch-Aramaisch, Englisch, Estnisch, Finnisch, Franzoesisch, Gotisch, Griechisch, Hawaiisch, Hebraisch, Hindustanisch, Italienisch, Kannada, Koptisch, Kurdisch, Latein, Lettisch, Litauisch, Malaiisch, Malayalam, Maltesisch, Neugriechisch, Persisch, Pali, Portugiesisch, Prakrit, Russisch, Samaritanisch, Sanskrit, Schwedisch, Spanisch, Syrisch, Tamil, Telugu, Tschagataisch, Tuerkisch.
Er uebersetzte unter anderem den Koran, und zwar auf eine ganz besondere Art und Weise: nicht wortwoertlich, sondern lyrisch. So entstanden Texte, in denen der Wohlklang der Koransuren, der literarische und dichterische Reiz deutlich wurden.]]
		messages ["de"] [88] = "Der Zettel scheint trockener. Ich glaube, ich warte hier noch kurz, bis der Zettel trocken ist!"
		messages ["de"] [89] = "Wir haben schon fast alle Farben befreit, aber die Farbsoldaten des Kommandeur Braun sind ja immer noch unterwegs und klauen weiter Farben. So wie die abgeschlagenen Typen an dem Standbild werden wir enden, wenn wir den Kommandanten nicht bald zu fassen bekommen!"
		messages ["de"] [90] = [[11. Reiterstandbild:

Seite 1/2:
Das Reiterstandbild zeigt den Markgrafen Christian Ernst, und wurde in den Jahren 1711/12 geschaffen. Zur Fertigung wurde ein Sandsteinblock aus den Steinbruechen am Burgberg gehauen.
Das Denkmal erinnert an die Teilnahme des Markgrafen an den "Tuerkenkriegen" und zeigt ihn auf dem Pferd in voller Ruestung. Unter den Tuerkenkriegen versteht man die Kriege zwischen dem sich nach dem Untergang von Byzanz im Jahre 1453 nach Norden und Westen ausbreitenden Osmanischen Reich und dem christlich gepraegten Europa.]]
		messages ["de"] [91] = [[Seite 2/2:
Die Osmanen waren jedoch bei weitem nicht nur Muslime, sondern auch viele Menschen, die dem orthodoxen Christentum angehoerten. Es war also viel mehr ein Krieg um Vormachtsstellungen und Handelswege, als ein religioes motivierter Krieg.
Unter dem Reiterstandbild von Markgraf Christian Ernst liegen ein geschlagener Osmane sowie Invidia, die Personifikation des Neids.]]
		messages ["de"] [92] = [[Folge den Soldaten! 1/4

Da vorn laufen die Farbsoldaten. Schnell hinterher!]]
		messages ["de"] [93] = "Es scheppert. Au!"
		messages ["de"] [94] = "Du bist gestolpert. Huch, was ist denn das? Das muessen die Soldaten auf Ihrer Flucht vor dir verloren haben! Schau dir das an!"
		messages ["de"] [95] = "Folge den Soldaten! 3/4"
		messages ["de"] [96] = "Folge den Soldaten! 4/4"
		messages ["de"] [97] = "Du siehst links auf der anderen Strassenseite eine eckige Steinsaeule. Schau sie dir mal an. Druecke dann auf OK!"
		messages ["de"] [98] = "Scheinbar hast du falsche Werte. Du kannst, wenn du die Werte jetzt richtig wissen solltest auch von hier Peilen. Das Geraet peilt von Stage 14 aus!"
		messages ["de"] [99] = "Du: 'Wisst ihr, wie ich diesen Brunnen zum Laufen kriege?'"
		messages ["de"] [100] = "Figuren: 'Klar, du musst die drei Hebel richtig stellen. Wir kommen dummerweise nicht ran.'"
		messages ["de"] [101] = "Du. 'Ach was. Na dann.'"
		messages ["de"] [102] = "Figuren: 'Der Gaertner sagt immer so einen Merksatz: Baum Gaensebluemchen Baum. Vielleicht kannst du damit etwas anfangen?'"
		messages ["de"] [103] = "Mist, entwischt, aber ich haette ihn im Gemeindefest was hier um die Ecke stattfindet wahrscheinlich eh verloren. Vielleicht weis der Geistliche in der Kirche mehr."
		messages ["de"] [104] = "Bibliothekar: Schau einer an, gerade wurde dein vorbestelltes Buch zurueckgegeben! Es hat sogar noch Farbe."
		messages ["de"] [105] = "Stand das nicht auf der Fassade?"
        messages ["de"] [106] = "Du: Ist es vielleicht ein "
        messages ["de"] [107] = "Du: Sind es vielleicht "
        messages ["de"] [108] = [[?
Frau: Ja, danke! Das war es! Ich werde allmaehlich wirklich vergesslich. Kann ich dir helfen?
Du: Ja, bitte, ich suche jemanden, der etwas mit der Farbe gelb zu tun hat...
Frau: Ach ja, da war ein gelber Soldat. Der fiel auf in all dem Grau hier! Der war hier ganz gestresst... hat was von Jakob Herz gemurmelt. Er ist zum Brunnen auf der anderen Seite des Platzes gerannt. ]]
        messages ["de"] [109] = [[5. Hugenottenkirche / Zuzug der Hugenotten:

Seite 1/3:
Die ersten Zuwanderer haben das Stadtbild entscheidend gepraegt: Die Gruendung der "Hugenottenstadt" im Jahr 1686 erkennt man noch heute in den schachbrettartig angelegten Haeuserzeilen der Neustadt. Zirka 1500 Hugenotten (oder auch Calivinisten) kamen in dem Jahr nach Erlang(en). Markgraf Christian Ernst bot den Fluechtlingen das Recht auf Ansiedlung in seinem noch an den Folgen des Dreissigjaehrigen Krieges leidenden Fuerstentum an, um dessen Wirtschaft durch die Ansiedlung moderner Gewerbe zu foerdern. ]]
        messages ["de"] [110] = [[Seite 2/3:
Er war damit einer der ersten lutherischen Fuersten in Deutschland, der Calvinisten in seinem Land aufnahm und ihnen sogar freie Religionsausuebung zusicherte. Noch bevor abzusehen war, mit wievielen Fluechtlingen gerechnet werden konnte, beschloss der Markgraf, suedlich der Altstadt eine eigenstaendige Siedlung (die "Neustadt Erlangen") zu gruenden. Mit dem rationalen Motiv, die Wirtschaft des eigenen Landes zu foerdern, verband sich in fuer den Absolutismus typischer Weise, die Hoffnung auf den Ruhm als Stadtgruender.]]
        messages ["de"] [111] = [[Seite 3/3:
Die Neustadt wurde nicht nur zum Ziel der Hugenotten, sondern auch von Lutheranern und Deutsch-Reformierten, denen jeweils dieselben Privilegien wie den Hugenotten verliehen worden waren. 1698 lebten 1000 Hugenotten und 317 Deutsche in Erlangen. Aufgrund der weiteren Zuwanderung wurden die Hugenotten jedoch bald zu einer franzoesisch sprechenden Minderheit in einer deutschen Stadt. Der franzoesische Einfluss nahm in der Folgezeit weiter ab. So wurde 1822 zum letzten Mal ein Gottesdienst in der Hugenottenkirche in franzoesischer Sprache gehalten.]]
        messages ["de"] [112] = "Das Tier ist staendig einmal an der Kirche zu sehen, es kann aber vorkommen, dass dieses Tier auch haeufiger zu sehen ist."
        messages ["de"] [113] = "Du solltest dich in der Realitaet nochmal umschauen!"
        messages ["de"] [114] = "Vogel"
        messages ["de"] [115] = "Taube"
        messages ["de"] [116] = "Tauben"
        messages ["de"] [117] = "Voegel"
        messages ["de"] [118] = [[Du musst "Vater Unser" noch 10 mal eingeben! (Nach jedem mal "Vater Unser" Answer druecken)]]
        messages ["de"] [119] = [[Du hast dich nun der Suende des Hochmuts Schuldig gemacht! Denn kein Mensch ist ohne Suende! Gebe nun um diese Suende abzuarbeiten 10 mal "Vater Unser" ein!]]
        messages ["de"] [120] = [[Naja dir scheint echt nicht mehr zu helfen zu sein! Ich gewaehre dir Mengenrabatt, er laesst dich nur 10 mal "Vater Unser" beten!]]
        messages ["de"] [121] = [[Du musst "Vater Unser" noch ]]
        messages ["de"] [122] = [[ mal eingeben! (Nach jedem mal "Vater Unser" Answer druecken)]]
        messages ["de"] [123] = "Nun ja, du scheinst ein frommer Mensch zu sein! Bete nun "
        messages ["de"] [124] = [[ mal "Vater Unser"!]]
        messages ["de"] [125] = "Elf"
        messages ["de"] [126] = "Die Tuer geht auf. Der Pfarrer kommt raus und faengt an dir von der Geschichte seiner Kirche zu erzaehlen. Hier ein kleiner Ausschnitt!"
        messages ["de"] [127] = [[7. Evangelisch-Lutherische Kirche gestern und heute:

Seite 1/2:
Die Kirchen haben in der deutschen Geschichte leider immer wieder sehr fragwuerdige Positionen vertreten. Auch in Erlangen wurden von einzelnen Theologen nationalistische und judenfeindliche Gedanken unterstuetzt. 1933 entwarf der lutherische Theologe Paul Althaus mit seinem Kollegen Werner Elert das Gutachten der Erlanger Theologischen Fakultaet zu einem geplanten Arierparagraphen der Reichskirche. Althaus und Elert fordern in ihrem Gutachten, "nichtarische" Bewerber um ein kirchliches Amt auszuschliessen. Bereits eingestellte "Nichtarier" sollten jedoch - entgegen den Forderungen der Deutschen Christen - nicht aus ihren Aemtern entlassen werden. Erst 1936 beginnt Althaus, sich von den Nationalsozialisten zu distanzieren.]]
        messages ["de"] [128] = [[Seite 2/2:
Heute ist die Kirche vielerorts aktive Gestalterin von Friedensprozessen. Vom Dritte-Welt-Laden, der faire Bezahlung und Arbeitsbedingungen foerdert, bis zum Friedensweg der Religionen in Erlangen, in dem alle Religionsgemeinschaften zusammenarbeiten, um Friedensprozesse zu foerdern. Dieser Friedensweg der Religionen entwickelte auch die Idee einer Stadtfuehrung "Offen aus Tradition in Erlangen", die zum Ausgangspunkt fuer "Die Jagd nach dem Gelben Soldaten" wurde.]]
        messages ["de"] [129] = [[Ihr schaut euch nun lange schweigend an. Was willst du jetzt tun?

Ich frage nach dem gelben Soldaten.
Ich beichte meine Suenden.]]
        messages ["de"] [130] = "Dir faellt auf, dass deine Tasche sehr schwer ist. Du ziehst das gelbe Bibliotheksbuch heraus und schlaegst die erste Seite auf, die der Pfarrer auch sieht."
        messages ["de"] [131] = "Pfarrer: Du solltest nun auf jeden Fall deine Suenden beichten! Du bist sicher kein Unschuldslamm wenn du schon deine Buecher fast nicht rechtzeitig abgeben kannst!"
        messages ["de"] [132] = "Ich glaube, hier brauchst du keinen Hinweis, dass schaffst du auch PRIMa ohne!"
        messages ["de"] [133] = [[Pfarrer: Von solchen Soldaten hoert man in letzter Zeit viel. Aber von solchen Themen habe ich leider wenig Ahnung.
Magst du vielleicht deine Suenden beichten? Davon habe ich mehr Ahnung.]]
        messages ["de"] [134] = "Fragen"
        messages ["de"] [135] = "Beichten"
        messages ["de"] [136] = "Auf dem Boden steht eine (virtuelle) Inschrift die diesen Ort erklaeren soll."
        messages ["de"] [137] = [[6. Pinnadeln in Erlangen:
Die Erlanger Kuenstlerin Isi Kunath hat im Jahr 2002 insgesamt 16 verschwundene historische Orte und Denkmale im ganzen Stadtgebiet mit grossen rotkoepfigen Pin-Nadeln zu markiert. Das Kunstwerk zur 1000-Jahr-Feier wurde am Donnerstag, 11. April 2002, auf dem Hugenottenplatz vollendet und offiziell uebergeben. Eigentlich sollten die Pin-Nadeln und die dazugehoerigen Info-Tafeln nur fuer ein Jahr aufgestellt werden, aber sie wurden nach anfanglichen Zweifeln in der Bevoelkerung akzeptiert. Im Herbst 2015 werden sie aufgrund der Material-Abnutzung auf Anraten der Kunstkommission der Stadt Erlangen endgueltig abgebaut.]]
        messages ["de"] [138] = "Diese ehemalige Stecknadel solltest du jetzt dringend mitnehmen!"
        messages ["de"] [139] = [[Gebuehrenden Dank gilt denen die dieses Projekt ins Leben gerufen haben!
Natuerlich danke ich auch der evangelischen Kirche, die die Final-Location genehmigt hat!]]
        messages ["de"] [140] = [[Der Unlockcode ist zum loggen auf wherigo.com notwendig. Er lautet:
]]
        messages ["de"] [141] = [[
Falls er nicht funktionieren sollte lasst die letzte Stelle weg.]]
        messages ["de"] [142] = "Oh, da ist ein Lesezeichen im Buch. Schaut aus, wie ein Merkzettel von jemandem, der auf gelb steht. "
        messages ["de"] [143] = "Endlich! Du hast das Denkmal von Jakob Herz gefunden."
        messages ["de"] [144] = [[12. Jakob-Herz-Denkmal:

Seite 1/2:
Jakob Herz (* 2. Februar 1816 in Bayreuth; + 27. September 1871 in Erlangen) war Arzt und spaeter erster juedischer Professor in Bayern bzw. Erlangen. Seine Verdienste waren weniger im wissenschaftlichen (seine Doktorarbeit hatte Klumpfuesse zum Inhalt) als vielmehr im zwischenmenschlichen Bereich angesiedelt. Aufgrund seiner Beliebtheit als leidenschaftlicher Lehrer und ueberzeugter Arzt haben Freunde und PatientInnen ihm zu Ehren gleich nach seinem Tod eine Statue errichtet; die erste fuer einen Juden in Bayern. ]]
        messages ["de"] [145] = [[Seite 2/2:
Diese wurde am 6. Mai 1875 in Form einer auf einem Steinsockel stehenden doppeltlebensgrossen Bronzestatue, die Herz im Gehrock darstellte, feierlich enthuellt. Sein Standort war der Holzmarkt, der heutige Hugenottenplatz, wo sich damals eine Schule befand. Am 14. September 1933 beschloss die NS-Stadtratsfraktion in Erlangen unter Vorsitz des Oberbuergermeisters Hans Flierl, "eine Kulturschande, die das ganze deutsche Volk als solche empfinden musste, wieder gutzumachen" , sprich das Herz-Denkmal auf dem damaligen Luitpoldplatz zu entfernen. 1983 wurde die Stele zwischen Krankenhaus- und Universitaetsstrasse errichtet, die "Ein Denkmal an ein Denkmal" darstellen und an die urspruengliche Statue erinnern sollte.]]
        messages ["de"] [146] = [[Du warst jetzt ein wenig langsam, die Farbsoldaten haben jetzt einen kleinen Vorsprung bekommen. Offensichtlich haben sie sich im Farbgefaengnis verschanzt. Es liegt nun an dir das Farbgefaegnis zu stuermen.

Du findest es in:
Meter: Die Summe aller Ziffern auf der Rueckseite plus vier
Grad: Die groesste Jahreszahl auf der Rueckseite minus die kleinste Zahl minus sieben]]
        messages ["de"] [147] = [[Oh nein, das Kaestchen hat ein Schloss!

Du bist inzwischen wieder vor die Bibliothek getreten.]]
        messages ["de"] [148] = "Du stehst vor der Bibliothek und hoerst ein seltsames Geraeusch. Huhuh huhuh huhuh. "
        messages ["de"] [149] = "Langsam siehst du nach oben. Dort sitzt eine gigantische Steineule."
        messages ["de"] [150] = [[Eule Eureka: Hallo, ich bin die Eule Eureka.
Das Kaestchen in deiner Hand ist meines. Ich habe es vor dem Zugriff der Farbsoldaten des Kommandeur Braun in der Bibliothek versteckt.
Mein hochsensibles Gespuer sagt mir, dass du den Soldaten ein Ende machen willst, deshalb gebe ich dir einen Tipp, wie du das Kaestchen oeffnen kannst.]]
        messages ["de"] [151] = [[Eule Eureka: Nachdem die Farbsoldaten nicht so unendlich clever wie Weltretter sind, habe ich in meiner grossen Eulenweisheit das Kaestchen mit einem akustischen Schloss versehen, das nur auf die richtigen Woerter aufgeht, die zu erraten sind.
In der gefuehlten Unendlichkeit, die ich als Steineule schon hier sitze, habe ich mir mit meiner unendlichen Raffiniertheit ein fast unendlich schwieriges Anagramm ausgedacht fuer einen Mann, der niemals das REICH STAERKEN wollte.]]
        messages ["de"] [152] = [[4. Palais Stutterheim: Zusammentreiben der Juden / Deportation:

Seite 1/3:
Amtshauptmann Christian Hieronymus Freiherr von Stutterheim begann 1728 mit dem Bau des Palais, das durch seine schlichte Schoenheit des stattlichen Baus besticht. Nach mehreren Besitzerwechseln ging das Palais im Jahr 1836 an die Stadt Erlangen ueber. Das Haus wurde zum Rathaus und beherbergte u.a. das Wachzimmer der Landwehr und die Kreissparkasse.]]
        messages ["de"] [153] = [[Seite 2/3:

Bei dem in Erlangen als "Judenaktion" bezeichneten Pogrom in der Nacht vom 9. auf den 10. November 1938 spielte das Gebaude als Rathaus und Amtssitz der Polizei eine beschaemende Rolle. Wahrend sich die SA auf dem Marktplatz sammelte, wurden auch die dienstfreien Beamten der Schutz- und Kriminalpolizei zur Wache gerufen. Im Rathaus organisierten der Oberbuergermeister Alfred Gross (1934 - 1944), der Leiter der Polizei Egidius Wolf und der SA-Sturmbannfuehrer Otto Klein mit anderen das Vorgehen. Dann wurden aus je einem Polizeibeamten und zwei SA-Maennern bestehende Trupps gebildet, die ab etwa 2 Uhr die etwa 43 Erlanger Juden in ihren Wohnungen verhafteten und alle - Kinder, Jugendliche, Erwachsene, alte Leute, Maenner und Frauen - zum Rathaus brachten.]]
        messages ["de"] [154] = [[Seite 3/3:

Waehrend ihre Wohnungen teilweise nach Wertpapieren durchsucht wurden, standen die Erlanger Juden zusammen mit drei Leidensgaefahrten aus Baiersdorf und sieben aus Fuerth stundenlang bei Kaelte und stroemendem Regen im Hof des Rathauses (also des Palais Stutterheim). Nur Alte und Gebrechliche sowie die Kinder durften in den Zellen bleiben.
Gegen Mittag schaffte man die juedischen Frauen und Kinder fuer mehrere Tage in das staedtische Obdachlosenasyl in der Woehrmuehle. Die Maenner kamen ins Amtsgerichtsgefangnis in der Adolf-Hitler-Str. 14, von dort wurden sie am naechsten Tag ins Gefaengnis nach Nuernberg ueberstellt.]]
        messages ["de"] [155] = "Mit der Pinnnadel zerstichst du den Luftballon!"
        messages ["de"] [156] = [[Die rote Farbe ist aus ihrem Gefaengnis befreit! Dabei kommen einige Farbspritzer auf die Kirchentuer.

Die rote Farbe ist sogar in dein Muldi-Dool und deine Stecknadel zurueckgekehrt!]]
        messages ["de"] [157] = "Mit dem Messer am Muldi-Dool zerstichst du den Luftballon!"
        messages ["de"] [158] = "Wie soll das den funktionieren?"
        messages ["de"] [159] = "Da! Der gelbe Soldat! Hinterher!"
        messages ["de"] [160] = "Du hoerst Musik von innen, aber keiner reagiert auf dein Klopfen. Vielleicht hoeren die nur auf ein bestimmtes Zeichen? Die Zahlen sehen mir nach einer bestimmten Reihe aus, muss ich die vielleicht fortsetzen?"
        messages ["de"] [161] = "X-mal anklopfen"
        messages ["de"] [162] = "Zettel mit militaerischem Aktionsplan"
        messages ["de"] [163] = "Die Entfernung zur naechsten Zone ist: "
        messages ["de"] [164] = "m."
        messages ["de"] [165] = "Vater Unser"
		messages ["de"] [166] = "Dir seien nun alle deine Suenden, die du begangen hast vergeben! Bitte begehe aber keine neuen, die neue Lastwagenladung Ablassbriefe kommt erst naechste Woche mit dem LKW an! Um dir die erste neue Suende zu ersparen gebe bitte dein Buch ab, weil sonst die Ausleihfrist verfallen wuerde."
		messages ["de"] [167] = [[Du musst "Vater Unser" noch ]]
		messages ["de"] [168] = [[ mal eingeben! (Nach jedem mal "Vater Unser" Answer druecken)]]
		messages ["de"] [169] = "Du hast dich vertippt! Hoffentlich machst du es beim naechsten mal wieder besser! Diese Eingabe zaehlt nicht als Versuch!"
		messages ["de"] [170] = [[Eule Eureka: Oh, du bist soooo klug.

K! L! U! K!]]
		messages ["de"] [171] = [[Gruene Farbe sprudelt aus der Kiste und faerbt das graue Gras in deiner Naehe wieder gruen.
Auf dem Boden der Kiste liegt ein Flyer.]]
		messages ["de"] [172] = "Eule Eureka: Da musst du wohl schaerfer nachdenken."
		messages ["de"] [173] = "Super! Du darfst nun ueber die Schablone mit Meterangaben, die sich in der Beschreibung des Items befinden, die naechste Zone suchen!"
		messages ["de"] [174] = "Da du scheinbar noch Probleme hast, wirst du nun zurueckgeleitet!"
		messages ["de"] [175] = "Schablone"
		messages ["de"] [176] = "Ich habs geloest"
		messages ["de"] [177] = "Gratuliere. Dann gebe deine Loesung nun ein!"
		messages ["de"] [178] = "Ein kleiner Hinweis bitte"
		messages ["de"] [179] = "Koennte es sein, dass der Zahlencode vielleicht mit Buchstaben in deiner Umgebung zusammenhaengt?"
		messages ["de"] [180] = "Ein grosser Hinweis bitte"
		messages ["de"] [181] = "Vor deiner Nase steht doch bestimmt ein Info-Schild in lesbarer Schrift. Koennte es sein, dass der Zahlencode etwas mit Zeilen, Woertern und Buchstaben darauf zu tun hat?"
		messages ["de"] [182] = "Loesung! Sofort!"
		messages ["de"] [183] = "Na na na, wir sind doch nicht Kommander Braun sondern ein*e Weltretter*in mit Manieren und total klug und loesen unsere Raetsel selber."
		messages ["de"] [184] = "Folge den Soldaten"
		messages ["de"] [185] = "Is klar! In Zukunft nehmen wir den Schuh als stylische Kopfbedeckung. Jetzt probiere bitte mal was ernsthaftes!"
		messages ["de"] [186] = "Geschichtsbuch"
		messages ["de"] [187] = "Ok, wenn du das Geschichtsbuch weiter so bearbeitest, dann geht es kaputt, also lass es lieber und probiere was anderes!"
		messages ["de"] [188] = "Stolperfalle"
		messages ["de"] [189] = [[Du zerschneidest mit dem Muldi-Dool die Schnuere!

Und wieder hast du die Welt ein bisschen bunter gemacht. Auf dich kommt ein Typ zu, der alles von der Parkbank aus beobachtet hat. Er klopft dir auf die Schulter und sagt: This colour is beautiful! Thank you for bringing colours back into our town!
Verlegen dankst du ihm... deine Wangen werden ganz orange, aehh, rot vor lauter Verlegenheit.]]
		messages ["de"] [190] = "Folge den Soldaten! 2/4"
		messages ["de"] [191] = "Bitte gebe eine Zahl ein!"
		messages ["de"] [192] = [[Du findest es in:
Meter: Die Summe aller Ziffern auf der Rueckseite plus vier
Grad: Die groesste Jahreszahl auf der Rueckseite minus die kleinste Zahl minus sieben

Aktuelle Werte:
- Meter: ]]
		messages ["de"] [193] = [[ m
- Grad: ]]
		messages ["de"] [194] = "*"
		messages ["de"] [195] = "Das ist vieeeeel zu weit weg fuer den Final! bitte gebe etwas kleineres ein!"
		messages ["de"] [196] = "Geschafft!"
        messages ["de"] [197] = [[Mit deinem grossem Spuersinn hast du es geschafft den versteckten Schluessel zu finden und in das Farbgefaengnis des Kommandeur Brauns einzudringen!

Und mit deinem grossen Heldenmut hast du es geschafft  Kommandeur Braun und seine Soldaten zu ueberwinden und in ihrem eigenen Farbgefaengnis fest zu setzen. Die Welt hat einen Schurken weniger - dank dir! Nie mehr sollen uns die Farben gestohlen werden.
Hoch lebe die Vielfalt unserer Welt!]]
        messages ["de"] [198] = "Wo ist das Gefaengnis?"
        messages ["de"] [199] = "Welcher Ort schaut denn hier wie ein Farbgefaengnis aus? Ihr muesst aber in keinem Fall auf das Gelaende!"
        messages ["de"] [200] = "OK"
        messages ["de"] [201] = "Spoilerbild"
        messages ["de"] [202] = "Wie oeffne ich das Farbgefaengnis?"
        messages ["de"] [203] = "Der Schluessel ist natuerlich gut gesichert. Die Zahlen zum Oeffnen des Verstecks solltest du in der richtigen Reihenfolge befreits haben. (Falls du sie nicht mehr weisst kannst du im Inventar nachschauen!)"
        messages ["de"] [204] = "Ich bin zu faul/muede zum suchen!"
        messages ["de"] [205] = "Dann hast du leider Pech, wo gibt?s denn so was? Vielleicht beginnst du den Where I Go nochmal mit jemandem, der mehr Lust zum Suchen hat!"
        messages ["de"] [206] = "HAUPTMENUE"
        messages ["de"] [207] = "Freiheit"
        messages ["de"] [208] = "Mit diesem Code kannst du deine Heldentat in der Ewigen Chronik der Farben in der Where-I-Go-Cartridge festhalten."
        messages ["de"] [209] = "Herzlichen Glueckwunsch, liebe*r "
        messages ["de"] [210] = [[
, du hast es geschafft! Wir danken dir, dass du mit deiner mutigen Tat die Farben wieder in die Welt gelassen hast!]]
        messages ["de"] [211] = "Sicher das du die Dose schon hast? Sicherheitshalber wirst du nun zur letzten Eingabe umgeleitet."
		messages ["de"] [212] = "Das ist leider der Falsche Weg! Versuche dir nochmal das Bild genau anzusehen und probiere einen anderen Weg."
		messages ["de"] [213] = "Du: Ist es vielleicht eine "
		messages ["de"] [214] = ""
    messages ["en"] = {}
        messages ["en"] [1] = ""
		messages ["en"] [2] = ""
		messages ["en"] [3] = ""
		messages ["en"] [4] = ""
		messages ["en"] [5] = ""
		messages ["en"] [6] = ""
		messages ["en"] [7] = ""
		messages ["en"] [8] = ""
		messages ["en"] [9] = ""
		messages ["en"] [10] = ""
		messages ["en"] [11] = ""
		messages ["en"] [12] = ""
		messages ["en"] [13] = ""
		messages ["en"] [14] = ""
		messages ["en"] [15] = ""
		messages ["en"] [16] = ""
		messages ["en"] [17] = ""
		messages ["en"] [18] = ""
		messages ["en"] [19] = ""
		messages ["en"] [20] = ""
		messages ["en"] [21] = ""
		messages ["en"] [22] = ""
		messages ["en"] [23] = ""
		messages ["en"] [24] = ""
		messages ["en"] [25] = ""
		messages ["en"] [26] = ""
		messages ["en"] [27] = ""
		messages ["en"] [28] = ""
		messages ["en"] [29] = ""
		messages ["en"] [30] = ""
		messages ["en"] [31] = ""
		messages ["en"] [32] = ""
		messages ["en"] [33] = ""
		messages ["en"] [34] = ""
		messages ["en"] [35] = ""
		messages ["en"] [36] = ""
		messages ["en"] [37] = ""
		messages ["en"] [38] = ""
		messages ["en"] [39] = ""
		messages ["en"] [40] = ""
		messages ["en"] [41] = ""
		messages ["en"] [42] = ""
		messages ["en"] [43] = ""
		messages ["en"] [44] = ""
		messages ["en"] [45] = ""
		messages ["en"] [46] = ""
		messages ["en"] [47] = ""
		messages ["en"] [48] = ""
		messages ["en"] [49] = ""
		messages ["en"] [50] = ""
		messages ["en"] [51] = ""
		messages ["en"] [52] = ""
		messages ["en"] [53] = ""
		messages ["en"] [54] = ""
		messages ["en"] [55] = ""
		messages ["en"] [56] = ""
		messages ["en"] [57] = ""
		messages ["en"] [58] = ""
		messages ["en"] [59] = ""
		messages ["en"] [60] = ""
		messages ["en"] [61] = ""
		messages ["en"] [62] = ""
		messages ["en"] [63] = ""
		messages ["en"] [64] = ""
		messages ["en"] [65] = ""
		messages ["en"] [66] = ""
		messages ["en"] [67] = ""
		messages ["en"] [68] = ""
		messages ["en"] [69] = ""
		messages ["en"] [70] = ""
		messages ["en"] [71] = ""
		messages ["en"] [72] = ""
		messages ["en"] [73] = ""
		messages ["en"] [74] = ""
		messages ["en"] [75] = ""
		messages ["en"] [76] = ""
		messages ["en"] [77] = ""
		messages ["en"] [78] = ""
		messages ["en"] [79] = ""
		messages ["en"] [80] = ""
		messages ["en"] [81] = ""
		messages ["en"] [82] = ""
		messages ["en"] [83] = ""
		messages ["en"] [84] = ""
		messages ["en"] [85] = ""
		messages ["en"] [86] = ""
		messages ["en"] [87] = ""
		messages ["en"] [88] = ""
		messages ["en"] [89] = ""
		messages ["en"] [90] = ""
		messages ["en"] [91] = ""
		messages ["en"] [92] = ""
		messages ["en"] [93] = ""
		messages ["en"] [94] = ""
		messages ["en"] [95] = ""
		messages ["en"] [96] = ""
		messages ["en"] [97] = ""
		messages ["en"] [98] = ""
		messages ["en"] [99] = ""
		messages ["en"] [100] = ""

function getMessage(language,number)
    return messages [language] [number]
end
