function C_AlteFrau_Cmd01()
    -- Ansprechen
    C_AlteFrau.Commands.Cmd01.Enabled = false
    C_AlteFrau.Commands.Cmd02.Enabled = true
    C_AlteFrau.Description = getMessage(language, 50)
end

function C_AlteFrau_Cmd02()
    -- Tier gefunden?
    I_TierHugenottenkirche:MoveTo(Player)
    C_AlteFrau.Description = ""
    Wherigo.GetInput(Q_TierHugenottenkirche)
end

function C_FriedrichAlexander_Cmd01()
    -- Ansprechen
    msgMitNachrichtUndBild(getMessage(language, 51) .. Player.Name .. getMessage(language, 52),
        P_Cacher,
        function()
            msgMitNachrichtUndBild(getMessage(language, 53),
                P_FreidrichAlexander,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 54),
                        P_Geschichtsbuch,
                        function()
                            C_FriedrichAlexander_Cmd01_2()
                        end)
                end)
        end)
end

function C_FriedrichAlexander_Cmd01_2()
    msgMitNachrichtUndBild(getMessage(language, 55),
        P_Geschichtsbuch,
        function()
            msgMitNachrichtUndBild(getMessage(language, 56),
                P_Stadtmotto,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 57),
                        P_Stadtmotto,
                        function()
                            KeepBook()
                        end)
                end)
        end)
end

function KeepBook()
    msgMitNachrichtUndButtons(getMessage(language, 58),
        getMessage(language, 48),
        getMessage(language, 49),
        function()
            msgMitNachrichtUndBild(getMessage(language, 59),
                P_FreidrichAlexander,
                function()
                    KeepBookFollowUp()
                end)
        end,
        function()
            msgMitNachrichtUndBild(getMessage(language, 60),
                P_FreidrichAlexander,
                function()
                    KeepBookFollowUp()
                end)
        end)
end

function KeepBookFollowUp()
    msgMitNachrichtUndBild(getMessage(language, 61),
        P_ErichKaestner,
        function()
            msgMitNachrichtUndBild(getMessage(language, 62),
                P_ErichKaestner,
                function()
                    msgMitNachricht(getMessage(language, 63),
                        function()
                            Stage01.Active = false
                            Stage02.Active = false
                            Stage03.Active = true
                            Stage03b.Active = true
                            T_FindeNaechsteZone.Description = getMessage(language, 64)
                            T_FindeNaechsteZone.Active = true
                            I_Geschichtsbuch:MoveTo(Player)
                            V_GeschichtsbuchMaxID = V_GeschichtsbuchMaxID + 2
                            V_GeschichtsbuchAngezeigterEintrag = 2
                            V_firstEnter = true
                            GeschichtbuchTexte()
                            Fortschritt()
                            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                        end)
                end)
        end)
end

function C_Statuen_Cmd01()
    -- Ansprechen
    msgMitNachrichtUndBild(getMessage(language, 99),
        P_Cacher,
        function()
            msgMitNachrichtUndBild(getMessage(language, 100),
                P_Statuen,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 101),
                        P_Cacher,
                        function()
                            msgMitNachrichtUndBild(getMessage(language, 102),
                                P_Statuen,
                                function()
                                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                                end)
                        end)
                end)
        end)
end