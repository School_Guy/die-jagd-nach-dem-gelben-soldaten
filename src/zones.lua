function Stage03b_OnEnter()
    msgMitNachricht(getMessage(language, 1),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage13b_OnEnter()
    msgMitNachricht(getMessage(language, 2),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage00_OnEnter()
    if (V_firstEnter) then
        V_firstEnter = false
        wideZone(Stage00, 0)
        msgMitNachricht(getMessage(language, 3),
            function()
                msgMitNachricht(getMessage(language, 4),
                    function()
                        I_Startfrage:MoveTo(Player)
                        Wherigo.GetInput(Q_Startabfrage)
                    end)
            end)
    end
end

function Stage01_OnEnter()
    -- Nothing to do
end

function Stage02_OnEnter()
    if (V_firstEnter) then
        V_firstEnter = false
        wideZone(Stage02, 0)
        Fortschritt()
        Stage01.Active = false
        Stage02.Visible = true
    end
end

function Stage03_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        T_FindeNaechsteZone.Active = false
        wideZone(Stage03, 0)
        msgMitNachricht(getMessage(language, 66),
            function()
                T_FindeNaechsteZone.Name = getMessage(language, 65)
                T_FindeNaechsteZone.Active = true
                Stage03b.Active = false
                Stage03.Visible = true
                I_Leseausweisfrage:MoveTo(Player)
                Wherigo.GetInput(Q_Leseausweis)
            end)
    end
end

function Stage04_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage04, 0)
        T_FindeNaechsteZone.Active = false
        I_Lesezeichen:MoveTo(nil)
    end
end

function Stage05_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage05, 0)
        T_FindeNaechsteZone.Active = false
    end
end

function Stage06_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage06, 0)
        T_FindeNaechsteZone.Active = false
        Cd_Speedstrecke56:Stop()
        msgMitNachricht(getMessage(language, 67),
            function()
                T_FindeNaechsteZone.Active = true
                Stage06.Active = false
                Stage07.Active = true
                Fortschritt()
                V_firstEnter = true
            end)
    end
end

function Stage07_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage07, 345)
        T_FindeNaechsteZone.Active = false
        msgMitNachricht(getMessage(language, 68),
            function()
                msgMitNachrichtUndBild(getMessage(language, 69),
                    P_GemeindefestLuftabllons,
                    function()
                        I_RoteLuftballons.Commands.Cmd01.Enabled = true
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    end
end

function Stage08_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage08, 0)
        T_FindeNaechsteZone.Active = false
        msgMitNachrichtUndBild(getMessage(language, 70),
            P_gelbesBibbuch,
            function()
                msgMitNachrichtUndButtons(getMessage(language, 71),
                    getMessage(language, 48),
                    getMessage(language, 49),
                    function()
                        msgMitNachricht(getMessage(language, 72),
                            function()
                                DropBookAtLibrarian()
                            end)
                    end,
                    function()
                        msgMitNachricht(getMessage(language, 73),
                            function()
                                DropBookAtLibrarian()
                            end)
                    end)
            end)
    end
end

function DropBookAtLibrarian()
    I_gelbesBibbuch:MoveTo(nil)
    I_Kaestchen:MoveTo(Player)
end

function Stage09_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage09, 0)
        T_FindeNaechsteZone.Active = false
        I_Flyer:MoveTo(nil)
        msgMitNachricht(getMessage(language, 74),
            function()
                StatueDialog1()
            end)
    end
end

function StatueDialog1()
    msgMitNachrichtUndBild(getMessage(language, 75),
        P_Steinfigur2SW,
        function()
            msgMitNachrichtUndBild(getMessage(language, 76),
                P_Steinfigur1SW,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 77),
                        P_Steinfigur2SW,
                        function()
                            StatueDialog2()
                        end)
                end)
        end)
end

function StatueDialog2()
    msgMitNachrichtUndBild(getMessage(language, 78),
        P_Steinfigur1SW,
        function()
            msgMitNachrichtUndBild(getMessage(language, 79),
                P_Steinfigur2SW,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 80),
                        P_CacherFarbspurGruen,
                        function()
                            msgMitNachrichtUndBild(getMessage(language, 81),
                                P_Steinfigur1SW,
                                function()
                                    HistoryBookEntryStatues()
                                end)
                        end)
                end)
        end)
end

function HistoryBookEntryStatues()
    msgMitNachrichtUndBild(getMessage(language, 82),
        P_SteinfigurenSchloss,
        function()
            msgMitNachrichtUndBild(getMessage(language, 83),
                P_SteinfigurenSchloss,
                function()
                    V_GeschichtsbuchAngezeigterEintrag = 7
                    V_GeschichtsbuchMaxID = V_GeschichtsbuchMaxID + 1
                    Stage09.Name = getMessage(language, 84)
                    I_Klappe:MoveTo(Stage09)
                    C_Statuen:MoveTo(Stage09)
                end)
        end)
end

function Stage10_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage10, 0)
        T_FindeNaechsteZone.Active = false
        msgMitNachrichtUndBild(getMessage(language, 85),
            P_Rueckertbrunnen,
            function()
                msgMitNachrichtUndBild(getMessage(language, 86),
                    P_Rueckert,
                    function()
                        msgMitNachrichtUndBild(getMessage(language, 87),
                            P_Rueckert,
                            function()
                                msgMitNachrichtUndBild(getMessage(language, 88),
                                    P_RueckertbrunnenZahlenNass,
                                    function()
                                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                                        Cd_RueckertWarten:Start()
                                    end)
                            end)
                    end)
            end)
    end
end

function Stage11_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage11, 250)
        T_FindeNaechsteZone.Active = false
        I_ZettelNass.Commands.Cmd02.Enabled = false
        I_ZettelNass:MoveTo(nil)
        Stage10.Active = false
        msgMitNachricht(getMessage(language, 89),
            function()
                msgMitNachrichtUndBild(getMessage(language, 90),
                    P_Reiterdenkmal,
                    function()
                        msgMitNachrichtUndBild(getMessage(language, 91),
                            P_Reiterdenkmal,
                            function()
                                msgMitNachrichtUndBild(getMessage(language, 92),
                                    P_SoldatenFolgen1,
                                    function()
                                        V_FolgeDenBildern_AnzahlAktiverBilder = V_FolgeDenBildern_AnzahlAktiverBilder + 1
                                        I_FolgeDenSoldaten:MoveTo(Player)
                                        T_FindeNaechsteZone.Active = true
                                        Stage11.Active = false
                                        Stage12.Active = true
                                        V_firstEnter = true
                                        Fortschritt()
                                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                                    end)
                            end)
                    end)
            end)
    end
end

function Stage12_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage12, 0)
        T_FindeNaechsteZone.Active = false
        Stage12.Active = false
        Stage12.Visible = true
        Stage12.Active = true
        Wherigo.PlayAudio(S_Stolpern)
        msgMitNachricht(getMessage(language, 93),
            function()
                msgMitNachricht(getMessage(language, 94),
                    function()
                        Stage12b.Active = true
                        Stage12c.Active = true
                        Stage12d.Active = true
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    end
end

function Stage12b_OnEnter()
    Stage12b.Visible = true
    msgMitNachricht(getMessage(language, 212),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage12c_OnEnter()
    Stage12c.Visible = true
    msgMitNachricht(getMessage(language, 212),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage12d_OnEnter()
    Stage12d.Visible = true
    msgMitNachricht(getMessage(language, 212),
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage13_OnEnter()
    if V_firstEnter then
        Stage12b.Active = false
        Stage12c.Active = false
        Stage12d.Active = false
        T_FindeNaechsteZone.Active = false
        Stage13.Visible = true
        msgMitNachrichtUndBild(getMessage(language, 95),
            P_SoldatenFolgen3,
            function()
                V_FolgeDenBildern_AnzahlAktiverBilder = V_FolgeDenBildern_AnzahlAktiverBilder + 1
            end)
    end
end

function Stage13_OnExit()
    msgMitNachrichtUndBild(getMessage(language, 96),
        P_SoldatenFolgen4,
        function()
            V_FolgeDenBildern_AnzahlAktiverBilder = V_FolgeDenBildern_AnzahlAktiverBilder + 1
            Stage13b.Active = true
            T_FindeNaechsteZone.Active = true
            Stage13.Active = false
            Stage14.Active = true
            V_firstEnter = true
            Fortschritt()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function Stage14_OnEnter()
    if V_firstEnter then
        V_firstEnter = false
        wideZone(Stage14, 350)
        T_FindeNaechsteZone.Active = false
        I_FolgeDenSoldaten:MoveTo(nil)
        Stage14.Visible = true
        Stage13b.Active = false
        msgMitNachrichtUndBild(getMessage(language, 97),
            P_JakobHerzDenkmal,
            function()
                V_firstEnter = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage15_OnProximity()
    if V_Richtig then
        if V_firstEnter then
            V_firstEnter = false
            T_FindeNaechsteZone.Active = false
            I_FinalGefunden:MoveTo(Player)
            I_BefreiteFarben:MoveTo(Player)
            I_AllesGepeilt:MoveTo(nil)
            Wherigo.GetInput(Q_FinalGefunden)
        end
    elseif not V_Richtig then
        msgMitNachricht(getMessage(language, 98),
            function()
                Stage14.Active = true
                Stage15.Active = false
                I_AllesGepeilt:MoveTo(Player)
            end)
    else
        msgMitNachricht("Fehler!", function()
        end)
    end
end