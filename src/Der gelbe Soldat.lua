require "global"
require "zones"
require "items"
require "characters"
require "questions"
require "timers"
require "messages"

language = "de"
cr = [[ 
]]

V_AktiverHebel = "Keiner"
V_firstEnter = true
V_GeschichtsbuchAngezeigterEintrag = 0
V_GeschichtsbuchID = 0
V_GeschichtsbuchMaxID = 0
V_GeschichtsbuchSchalter = 0
V_FarbeAngezeigt = 0
V_FarbenBefreit = 0
V_FolgeDenBildern_AnzahlAktiverBilder = 0
V_FolgeDenBildern_Anzeige = 1
V_Falschantworten = 0
V_TierFalsch = 0
V_AnzahlVaterUnser = 10
V_TuerklopfenAnzahl = 0
V_HebelA = "Nix"
V_HebelB = "Nix"
V_HebelC = "Nix"
V_Grad = 0
V_Meter = 0
V_Richtig = false
V_Fortschritt = 0

function Farben()
    if (V_FarbeAngezeigt > 4) then
        V_FarbeAngezeigt = 1
    elseif (V_FarbeAngezeigt <= 0) then
        V_FarbeAngezeigt = 4
    end
    if V_FarbeAngezeigt == 1 then
        I_BefreiteFarben.Media = P_FarbeBefreitRot
        I_BefreiteFarben.Description = getMessage(language, 7)
    elseif V_FarbeAngezeigt == 2 then
        I_BefreiteFarben.Media = P_GruenBefreitKiste
        I_BefreiteFarben.Description = getMessage(language, 8)
    elseif V_FarbeAngezeigt == 3 then
        I_BefreiteFarben.Media = P_FarbeBefreitBlau
        I_BefreiteFarben.Description = getMessage(language, 9)
    elseif V_FarbeAngezeigt == 4 then
        I_BefreiteFarben.Media = P_FarbeBefreitOrange
        I_BefreiteFarben.Description = getMessage(language, 10)
    else
        Wherigo.LogMessage("Invalid Image value.", Wherigo.LOGERROR)
    end
end

function FolgeDenSoldaten()
    if (V_FolgeDenBildern_AnzahlAktiverBilder < V_FolgeDenBildern_Anzeige) then
        V_FolgeDenBildern_Anzeige = 1
    elseif (V_FolgeDenBildern_Anzeige == 0) then
        V_FolgeDenBildern_Anzeige = V_FolgeDenBildern_AnzahlAktiverBilder
    else
        Wherigo.LogMessage("Invalid Image value.", Wherigo.LOGERROR)
    end

    if (V_FolgeDenBildern_Anzeige == 1) then
        I_FolgeDenSoldaten.Media = P_SoldatenFolgen1
        I_FolgeDenSoldaten.Icon = P_Icon_SoldatenFolgen1
        I_FolgeDenSoldaten.Description = getMessage(language, 11)
    elseif (V_FolgeDenBildern_Anzeige == 2) then
        I_FolgeDenSoldaten.Media = P_SoldatenFolgen2
        I_FolgeDenSoldaten.Icon = P_Icon_SoldatenFolgen2
        I_FolgeDenSoldaten.Description = getMessage(language, 12)
    elseif (V_FolgeDenBildern_Anzeige == 3) then
        I_FolgeDenSoldaten.Media = P_SoldatenFolgen3
        I_FolgeDenSoldaten.Icon = P_Icon_SoldatenFolgen3
        I_FolgeDenSoldaten.Description = getMessage(language, 13)
    elseif (V_FolgeDenBildern_Anzeige == 4) then
        I_FolgeDenSoldaten.Media = P_SoldatenFolgen4
        I_FolgeDenSoldaten.Icon = P_Icon_SoldatenFolgen4
        I_FolgeDenSoldaten.Description = getMessage(language, 14)
    end
end

function GeschichtsbuchSchalten()
    if (V_GeschichtsbuchSchalter == 1) then
        V_GeschichtsbuchAngezeigterEintrag = V_GeschichtsbuchAngezeigterEintrag - 1
    elseif (V_GeschichtsbuchSchalter == 2) then
        V_GeschichtsbuchAngezeigterEintrag = V_GeschichtsbuchAngezeigterEintrag + 1
    end
    V_GeschichtsbuchSchalter = 0
    GeschichtbuchTexte()
end

function GeschichtbuchTexte()
    if (V_GeschichtsbuchAngezeigterEintrag > V_GeschichtsbuchMaxID) then
        V_GeschichtsbuchAngezeigterEintrag = V_GeschichtsbuchMaxID
    end
    V_GeschichtsbuchID = V_GeschichtsbuchAngezeigterEintrag

    if (V_GeschichtsbuchID == 0) then
        I_Geschichtsbuch.Description = getMessage(language, 15)
        I_Geschichtsbuch.Image = P_Stadtmotto
    elseif (V_GeschichtsbuchID == 1) then
        I_Geschichtsbuch.Description = getMessage(language, 16)
        I_Geschichtsbuch.Image = P_Buecherverbrennung
    elseif (V_GeschichtsbuchID == 2) then
        I_Geschichtsbuch.Description = getMessage(language, 17)
        I_Geschichtsbuch.Image = P_ErichKaestner
    elseif (V_GeschichtsbuchID == 3) then
        I_Geschichtsbuch.Description = getMessage(language, 18)
        I_Geschichtsbuch.Image = P_PalaisStutterheim
    elseif (V_GeschichtsbuchID == 4) then
        I_Geschichtsbuch.Description = getMessage(language, 19)
        I_Geschichtsbuch.Image = P_Hugenottenkirche
    elseif (V_GeschichtsbuchID == 5) then
        I_Geschichtsbuch.Description = getMessage(language, 20)
        I_Geschichtsbuch.Image = P_Pinnadeln
    elseif (V_GeschichtsbuchID == 6) then
        I_Geschichtsbuch.Description = getMessage(language, 21)
        I_Geschichtsbuch.Image = P_RolleDerKirche
    elseif (V_GeschichtsbuchID == 7) then
        I_Geschichtsbuch.Description = getMessage(language, 22)
        I_Geschichtsbuch.Image = P_AlteUni
    elseif (V_GeschichtsbuchID == 8) then
        I_Geschichtsbuch.Description = getMessage(language, 23)
        I_Geschichtsbuch.Image = P_SteinfigurenSchloss
    elseif (V_GeschichtsbuchID == 9) then
        I_Geschichtsbuch.Description = getMessage(language, 24)
        I_Geschichtsbuch.Image = P_Rueckert
    elseif (V_GeschichtsbuchID == 10) then
        I_Geschichtsbuch.Description = getMessage(language, 25)
        I_Geschichtsbuch.Image = P_Reiterdenkmal
    elseif (V_GeschichtsbuchID == 11) then
        I_Geschichtsbuch.Description = getMessage(language, 26)
        I_Geschichtsbuch.Image = P_JakobHerz
    elseif (V_GeschichtsbuchID == 12) then
        I_Geschichtsbuch.Description = getMessage(language, 27)
        I_Geschichtsbuch.Image = P_FreuedenswegDerReligionen
    end
end

function Hebel()
    I_Hebel.Description = getMessage(language, 28) .. V_HebelA .. cr .. getMessage(language, 29) .. V_HebelB .. cr
            .. getMessage(language, 30) .. V_HebelC
    if (V_HebelA == getMessage(language, 31)
            and V_HebelB == getMessage(language, 32)
            and V_HebelC == getMessage(language, 31)) then
        if platform == "Android" or platform == "IPhone" then
            Wherigo.PlayAudio(S_FarbeBefreit)
        end
        msgMitNachricht(getMessage(language, 34),
            function()
                msgMitNachricht(getMessage(language, 35),
                    function()
                        msgMitNachricht(getMessage(language, 36),
                            function()
                                msgMitNachricht(getMessage(language, 37),
                                    function()
                                        Stage09.Active = false
                                        I_ZettelNass:MoveTo(Player)
                                        V_FarbenBefreit = V_FarbenBefreit + 1
                                        Cd_BrunnenWarten:Start()
                                    end)
                            end)
                    end)
            end)
    else
        if platform == "Android" or platform == "IPhone" then
            Wherigo.PlayAudio(S_KlickBrunnen)
        end
        msgMitNachricht(getMessage(language, 38),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Hebel)
            end)
    end
end

function FinalLegen()
    Stage14.Active = false
    if (V_Grad == 110 and V_Meter == 158) then
        V_Richtig = true
        Stage15.Active = true
        Stage15.Active = false
        Stage15.Points = {
            Wherigo.ZonePoint(49.59715, 11.012667, 0),
            Wherigo.ZonePoint(49.59715, 11.012667, 0),
            Wherigo.ZonePoint(49.59715, 11.012667, 0),
            Wherigo.ZonePoint(49.59715, 11.012667, 0)
        }
        Stage15.OriginalPoint = Wherigo.ZonePoint(49.59715, 11.012667, 0)
        Stage15.Active = true
        Fortschritt()
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    elseif (V_Meter == 0) then
        msgMitNachricht(getMessage(language, 39),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_AllesGepeilt)
            end)
    else
        Stage15.Active = true
        Stage15.Active = false
        refPoint = Wherigo.ZonePoint(49.5977064266858, 11.0107389122796, 0)
        local distance = Wherigo.Distance(V_Meter, 'm')
        MyZonePoint = Wherigo.TranslatePoint(refPoint, distance, V_Grad)
        Stage15.Points = { MyZonePoint, MyZonePoint, MyZonePoint, MyZonePoint }
        Stage15.OriginalPoint = MyZonePoint
        Stage15.Active = true
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function Fortschritt()
    V_Fortschritt = V_Fortschritt + 6.6
    if V_Fortschritt > 97 then
        V_Fortschritt = 100
    end
    T_Fortschritt.Name = getMessage(language, 5) .. round(V_Fortschritt, 2) .. getMessage(language, 6)
end

function round(num, numDecimalPlaces)
    local mult = 10^(numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
  end
