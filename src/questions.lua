function Q_Startfrage_OnGetInput(answer)
    if (answer == "") then
        return
    end

    answer = tonumber(answer)

    if (answer == 8031) then
        msgMitNachrichtUndBild(getMessage(language, 41),
            P_Cacher,
            function()
                I_Startfrage:MoveTo(nil)
                V_firstEnter = true
                Stage00.Active = false
                Stage01.Active = true
                Stage02.Active = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        V_Falschantworten = V_Falschantworten + 1
        if (V_Falschantworten == 1) then
            msgMitNachricht(getMessage(language, 42),
                function()
                    Wherigo.GetInput(Q_Startabfrage)
                end)
        elseif (V_Falschantworten == 2) then
            msgMitNachricht(getMessage(language, 43),
                function()
                    Wherigo.GetInput(Q_Startabfrage)
                end)
        elseif (V_Falschantworten == 3) then
            msgMitNachricht(getMessage(language, 44),
                function()
                    Wherigo.GetInput(Q_Startabfrage)
                end)
        end
    end
end

function Q_Leseausweis_OnGetInput(answer)
    if (answer == "") then
        return
    end

    answer = tonumber(answer)

    if answer == 1728 then
        I_Leseausweisfrage:MoveTo(nil)
        -- Sound Pling
        msgMitNachricht(getMessage(language, 104),
            function()
                V_GeschichtsbuchMaxID = V_GeschichtsbuchMaxID + 1
                V_GeschichtsbuchAngezeigterEintrag = 4
                I_gelbesBibbuch:MoveTo(Stage03)
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_gelbesBibbuch)
            end)
    else
        msgMitNachricht(getMessage(language, 105),
            function()
                Wherigo.GetInput(Q_Leseausweis)
            end)
    end
end

function Q_TierHugenottenkirche_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if Wherigo.NoCaseEquals(answer, getMessage(language, 114)) then
        local text = getMessage(language, 106) .. answer .. getMessage(language, 108)
        msgMitNachricht(text,
            function()
                HistoryBookHugenottenkircheFollowUp()
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 115)) then
        local text = getMessage(language, 213) .. answer .. getMessage(language, 108)
        msgMitNachricht(text,
            function()
                HistoryBookHugenottenkircheFollowUp()
            end)
    elseif (Wherigo.NoCaseEquals(answer, getMessage(language, 116)) or Wherigo.NoCaseEquals(answer, getMessage(language, 117))) then
        local text = getMessage(language, 107) .. answer .. getMessage(language, 108)
        msgMitNachricht(text,
            function()
                HistoryBookHugenottenkircheFollowUp()
            end)
    else
        if (V_TierFalsch > 3) then
            msgMitNachricht(getMessage(language, 112),
                function()
                    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, C_AlteFrau)
                end)
        else
            msgMitNachricht(getMessage(language, 113),
                function()
                    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, C_AlteFrau)
                end)
        end
    end
end

function HistoryBookHugenottenkircheFollowUp()
    msgMitNachrichtUndBild(getMessage(language, 109),
        P_Hugenottenkirche,
        function()
            msgMitNachrichtUndBild(getMessage(language, 110),
                P_Hugenottenkirche,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 111),
                        P_Hugenottenkirche,
                        function()
                            I_TierHugenottenkirche:MoveTo(nil)
                            V_GeschichtsbuchAngezeigterEintrag = 5
                            V_GeschichtsbuchMaxID = V_GeschichtsbuchMaxID + 1
                            Stage04.Active = false
                            Stage05.Active = true
                            V_firstEnter = true
                            C_AlteFrau.Commands.Cmd02.Enabled = false
                            T_FindeNaechsteZone.Active = true
                            Fortschritt()
                            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                        end)
                end)
        end)
end

function Q_Beichte_OnGetInput(answer)
    if (answer == "") then
        return
    end

    answer = tonumber(answer)
    -- Will not fail because nil == 0 --> false
    if (answer == 0) then
        I_Beichte:MoveTo(nil)
        Q_VaterUnser.Text = getMessage(language, 118)
        msgMitNachricht(getMessage(language, 119),
            function()
                I_VaterUnser:MoveTo(Player)
                Wherigo.GetInput(Q_VaterUnser)
            end)
    -- Will fail if tonumber() returns nil
    elseif (answer >= 10) then
        I_Beichte:MoveTo(nil)
        Q_VaterUnser.Text = getMessage(language, 118)
        msgMitNachricht(getMessage(language, 120),
            function()
                I_VaterUnser:MoveTo(Player)
                Wherigo.GetInput(Q_VaterUnser)
            end)
    elseif (answer > 0 and answer < 10) then
        I_Beichte:MoveTo(nil)
        V_AnzahlVaterUnser = answer
        Q_VaterUnser.Text = getMessage(language, 121) .. answer .. getMessage(language, 122)
        local text = getMessage(language, 123) .. answer .. getMessage(language, 124)
        msgMitNachricht(text,
            function()
                I_VaterUnser:MoveTo(Player)
                Wherigo.GetInput(Q_VaterUnser)
            end)
    else
        msgMitNachricht("Fehler",
        function()
            return
        end)
    end
end

function Q_Tuer_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if (answer == "11" or answer == getMessage(language, 125)) then
        I_Tuerraetsel:MoveTo(nil)
        I_Tuer:MoveTo(nil)
        I_Zettel:MoveTo(nil)
        Wherigo.PlayAudio(S_Kirchenmesse)
        msgMitNachrichtUndBild(getMessage(language, 126),
            P_PfarrerSW,
            function()
                msgMitNachrichtUndBild(getMessage(language, 127),
                    P_RolleDerKirche,
                    function()
                        msgMitNachrichtUndBild(getMessage(language, 128),
                            P_RolleDerKirche,
                            function()
                                V_GeschichtsbuchMaxID = V_GeschichtsbuchMaxID + 1
                                V_GeschichtsbuchAngezeigterEintrag = 6
                                HistoryBookFollowUpRoleOfTheChurch()
                            end)
                    end)
            end)
    else
        msgMitNachricht(getMessage(language, 132),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function HistoryBookFollowUpRoleOfTheChurch()
    msgMitNachrichtUndButtons(getMessage(language, 129),
        getMessage(language, 134),
        getMessage(language, 135),
        function()
            msgMitNachrichtUndButtonsUndBild(getMessage(language, 133),
                getMessage(language, 48),
                getMessage(language, 49),
                P_PfarrerSW,
                function()
                    I_Beichte:MoveTo(Player)
                    Wherigo.GetInput(Q_Beichte)
                end,
                function()
                    msgMitNachricht(getMessage(language, 130),
                        function()
                            msgMitNachricht(getMessage(language, 131),
                                function()
                                    Wherigo.GetInput(Q_Beichte)
                                end)
                        end)
                end)
        end,
        function()
            I_Beichte:MoveTo(Player)
            Wherigo.GetInput(Q_Beichte)
        end)
end

function Q_VaterUnser_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if (Wherigo.NoCaseEquals(answer, getMessage(language, 165))) then
        V_AnzahlVaterUnser = V_AnzahlVaterUnser - 1
        if (V_AnzahlVaterUnser == 0) then
            msgMitNachricht(getMessage(language, 166),
                function()
                    I_VaterUnser:MoveTo(nil)
                    Stage07.Active = false
                    Stage08.Active = true
                    V_firstEnter = true
                    T_FindeNaechsteZone.Active = true
                    Fortschritt()
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        else
            Q_VaterUnser.Text = getMessage(language, 167)
                    .. V_AnzahlVaterUnser
                    .. getMessage(language, 168)
            Wherigo.GetInput(Q_VaterUnser)
        end
    else
        msgMitNachricht(getMessage(language, 169),
            function()
                Wherigo.GetInput(Q_VaterUnser)
            end)
    end
end

function Q_Eulenraetsel_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if (Wherigo.NoCaseEquals(answer, "Erich Kaestner") or Wherigo.NoCaseEquals(answer, "ErichKaestner")) then
        I_Eulenraetsel:MoveTo(nil)
        V_FarbenBefreit = V_FarbenBefreit + 1
        msgMitNachrichtUndBild(getMessage(language, 170),
            P_Kluk,
            function()
                Wherigo.PlayAudio(S_FarbeBefreit)
                msgMitNachrichtUndBild(getMessage(language, 171),
                    P_GruenBefreitKiste,
                    function()
                        I_Kaestchen:MoveTo(nil)
                        I_Flyer:MoveTo(Player)
                        Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Flyer)
                    end)
            end)
    else
        msgMitNachricht(getMessage(language, 172),
            function()
                Wherigo.GetInput(Q_Eulenraetsel)
            end)
    end
end

function Q_Hebelraetsel_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if (answer == getMessage(language, 31)) then
        --oben
        if (V_AktiverHebel == "A") then
            V_HebelA = getMessage(language, 31)
        elseif (V_AktiverHebel == "B") then
            V_HebelB = getMessage(language, 31)
        elseif (V_AktiverHebel == "C") then
            V_HebelC = getMessage(language, 31)
        end
        Hebel()
    elseif (answer == getMessage(language, 33)) then
        --mitte
        if (V_AktiverHebel == "A") then
            V_HebelA = getMessage(language, 33)
        elseif (V_AktiverHebel == "B") then
            V_HebelB = getMessage(language, 33)
        elseif (V_AktiverHebel == "C") then
            V_HebelC = getMessage(language, 33)
        end
        Hebel()
    elseif (answer == getMessage(language, 32)) then
        --unten
        if (V_AktiverHebel == "A") then
            V_HebelA = getMessage(language, 32)
        elseif (V_AktiverHebel == "B") then
            V_HebelB = getMessage(language, 32)
        elseif (V_AktiverHebel == "C") then
            V_HebelC = getMessage(language, 32)
        end
        Hebel()
    elseif (answer == "HAUPTMENUE") then
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function Q_Buchcode_OnGetInput(answer)
    if (answer == "") then
        return
    end

    if (Wherigo.NoCaseEquals(answer, "Reiterstandbild")) then
        I_Raetselcode:MoveTo(nil)
        I_ZettelNass.Image = P_None
        I_ZettelNass.Icon = P_None
        msgMitNachricht(getMessage(language, 173),
            function()
                I_ZettelNass.Name = getMessage(language, 175)
                I_ZettelNass.Commands.Cmd01.Enabled = false
                I_ZettelNass.Commands.Cmd02.Enabled = true
                T_FindeNaechsteZone.Active = true
                Stage10.Active = false
                Stage11.Active = true
                V_firstEnter = true
                Fortschritt()
            end)
    else
        msgMitNachricht(getMessage(language, 174),
            function()
                Wherigo.GetInput(Q_ZettelBrunnen)
            end)
    end
end

function Q_ZettelBrunnen_OnGetInput(answer)
    if (answer == nil or answer == "") then
        return
    end
    if Wherigo.NoCaseEquals(answer, getMessage(language, 176)) then
        msgMitNachricht(getMessage(language, 177),
            function()
                Wherigo.GetInput(Q_Buchcode)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 178)) then
        msgMitNachricht(getMessage(language, 179),
            function()
                Wherigo.GetInput(Q_ZettelBrunnen)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 180)) then
        msgMitNachricht(getMessage(language, 181),
            function()
                Wherigo.GetInput(Q_ZettelBrunnen)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 182)) then
        msgMitNachricht(getMessage(language, 183),
            function()
                Wherigo.GetInput(Q_ZettelBrunnen)
            end)
    end
end

function Q_MuldiDool_OnGetInput(answer)
    if (answer == nil or answer == "") then
        return
    end
    if Wherigo.NoCaseEquals(answer, getMessage(language, 184)) then
        msgMitNachricht(getMessage(language, 185),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 186)) then
        msgMitNachricht(getMessage(language, 187),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 188)) then
        I_MuldiDool.Commands.Cmd01.Enabled = false
        Wherigo.PlayAudio(S_FarbeBefreit)
        msgMitNachrichtUndBild(getMessage(language, 189),
            P_FarbeBefreitOrange,
            function()
                msgMitNachrichtUndBild(getMessage(language, 190),
                    P_SoldatenFolgen2,
                    function()
                        V_FolgeDenBildern_AnzahlAktiverBilder = V_FolgeDenBildern_AnzahlAktiverBilder + 1
                        V_FarbenBefreit = V_FarbenBefreit + 1
                        Stage12.Active = false
                        Stage13.Active = true
                        I_MuldiDool:MoveTo(nil)
                        T_FindeNaechsteZone.Active = true
                        V_firstEnter = true
                        Fortschritt()
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    end
end

function Q_Grad_OnGetInput(answer)
    answer = tonumber(answer)
    if answer == nil then
        msgMitNachricht(getMessage(language, 191),
            function()
                Wherigo.GetInput(Q_Grad)
            end)
        return
    end

    V_Grad = answer
    I_AllesGepeilt.Description = getMessage(language, 192) .. V_Meter .. getMessage(language, 193)
            .. V_Grad .. getMessage(language, 194)
    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_AllesGepeilt)
end

function Q_Meter_OnGetInput(answer)
    answer = tonumber(answer)
    if answer == nil then
        msgMitNachricht(getMessage(language, 191),
            function()
                Wherigo.GetInput(Q_Grad)
            end)
        return
    end

    if answer > 300 then
        msgMitNachricht(getMessage(language, 195),
            function()
                Wherigo.GetInput(Q_Meter)
            end)
    else
        V_Meter = answer
        I_AllesGepeilt.Description = getMessage(language, 192) .. V_Meter .. getMessage(language, 193)
                .. V_Grad .. getMessage(language, 194)
        Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_AllesGepeilt)
    end
end

function Q_FinalGefunden_OnGetInput(answer)
    if (answer == nil or answer == "") then
        return
    end
    if Wherigo.NoCaseEquals(answer, getMessage(language, 196)) then
        I_FinalGefunden:MoveTo(nil)
        msgMitNachrichtUndBild(getMessage(language, 197),
            P_Colorful,
            function()
                I_RebusAusDose:MoveTo(Player)
                Wherigo.GetInput(Q_RebusAusDose)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 198)) then
        msgMitNachrichtUndButtons(getMessage(language, 199),
            getMessage(language, 200),
            getMessage(language, 201),
            function()
                Wherigo.GetInput(Q_FinalGefunden)
            end,
            function()
                msgMitNachrichtUndBild("", P_Spoiler,
                    function()
                        Wherigo.GetInput(Q_FinalGefunden)
                    end)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 202)) then
        msgMitNachrichtUndBild(getMessage(language, 203),
            P_Farbreihenfolge,
            function()
                Wherigo.GetInput(Q_FinalGefunden)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 204)) then
        msgMitNachricht(getMessage(language, 205),
            function()
                Wherigo.GetInput(Q_FinalGefunden)
            end)
    elseif Wherigo.NoCaseEquals(answer, getMessage(language, 206)) then
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function Q_RebusAusDose_OnGetInput(answer)
    if (answer == nil or answer == "") then
        return
    end

    if Wherigo.NoCaseEquals(answer, getMessage(language, 207)) then
        I_RebusAusDose:MoveTo(nil)
        msgMitNachricht(getMessage(language, 208),
            function()
                Wherigo.PlayAudio(S_Heldensound)
                local text = getMessage(language, 209) .. Player.Name .. getMessage(language, 210)
                msgMitNachricht(text,
                    function()
                        I_Final:MoveTo(Player)
                        Fortschritt()
                    end)
            end)
    else
        msgMitNachricht(getMessage(language, 211),
            function()
                Wherigo.GetInput(Q_FinalGefunden)
            end)
    end
end
