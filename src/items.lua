function I_AllesGepeilt_Cmd01()
    -- Meter
    Wherigo.GetInput(Q_Meter)
end

function I_AllesGepeilt_Cmd02()
    -- Grad
    Wherigo.GetInput(Q_Grad)
end

function I_AllesGepeilt_Cmd03()
    -- Fertig
    msgMitNachrichtUndButtons(getMessage(language, 45) .. V_Meter .. getMessage(language, 46) .. V_Grad
            .. getMessage(language, 47),
        getMessage(language, 48),
        getMessage(language, 49),
        function()
            T_FindeNaechsteZone.Active = true
            FinalLegen()
        end,
        function()
            Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_AllesGepeilt)
        end)
end

function I_BefreiteFarben_Cmd01()
    --Zurueck
    V_FarbeAngezeigt = V_FarbeAngezeigt - 1
    Farben()
end

function I_BefreiteFarben_Cmd02()
    --Weiter
    V_FarbeAngezeigt = V_FarbeAngezeigt + 1
    Farben()
end

function I_Brunnen_Cmd01()
    --OK
    msgMitNachricht(getMessage(language, 136),
        function()
            msgMitNachrichtUndBild(getMessage(language, 137),
                P_Pinnadeln,
                function()
                    msgMitNachricht(getMessage(language, 138),
                        function()
                            I_Brunnen:MoveTo(nil)
                            I_Stecknadel:MoveTo(Stage05)
                        end)
                end)
        end)
end

function I_Final_Cmd01()
    --Dank an
    msgMitNachricht(getMessage(language, 139), nil)
end

function I_Final_Cmd02()
    --Unlock-Code
    local text = getMessage(language, 140) .. Player.UnlockCode .. getMessage(language, 141)
    msgMitNachricht(text, nil)
end

function I_Flyer_Cmd01()
    --Flyer gelesen?
    I_Flyer.Description = ""
    I_Flyer.Commands.Cmd01.Enabled = false
    Stage09.Active = true
    V_firstEnter = true
    T_FindeNaechsteZone.Active = true
    Fortschritt()
    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
end

function I_FolgeDenSoldaten_Cmd01()
    --Weiter
    V_FolgeDenBildern_Anzeige = V_FolgeDenBildern_Anzeige + 1
    FolgeDenSoldaten()
end

function I_FolgeDenSoldaten_Cmd02()
    --Zurueck
    V_FolgeDenBildern_Anzeige = V_FolgeDenBildern_Anzeige - 1
    FolgeDenSoldaten()
end

function I_gelbesBibbuch_Cmd01()
    msgMitNachricht(getMessage(language, 142),
        function()
            I_gelbesBibbuch:MoveTo(Player)
            I_Lesezeichen:MoveTo(Player)
            I_gelbesBibbuch.Commands.Cmd01.Enabled = false
            Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Lesezeichen)
        end)
end

function I_Geschichtsbuch_Cmd01()
    --Weiter
    V_GeschichtsbuchSchalter = 2
    GeschichtsbuchSchalten()
end

function I_Geschichtsbuch_Cmd02()
    --X
    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
end

function I_Geschichtsbuch_Cmd03()
    --Zurueck
    V_GeschichtsbuchSchalter = 1
    GeschichtsbuchSchalten()
end

function I_Hebel_Cmd01()
    --1
    V_AktiverHebel = "A"
    Wherigo.GetInput(Q_Hebelraetsel)
end

function I_Hebel_Cmd02()
    --2
    V_AktiverHebel = "B"
    Wherigo.GetInput(Q_Hebelraetsel)
end

function I_Hebel_Cmd03()
    --3
    V_AktiverHebel = "C"
    Wherigo.GetInput(Q_Hebelraetsel)
end

function I_HerzDenkmal_Cmd01()
    --OK
    I_HerzDenkmal.Commands.Cmd01.Enabled = false
    msgMitNachricht(getMessage(language, 143),
        function()
            msgMitNachrichtUndBild(getMessage(language, 144),
                P_JakobHerz,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 145),
                        P_JakobHerz,
                        function()
                            msgMitNachrichtUndBild(getMessage(language, 146),
                                P_JakobHerzDenkmal,
                                function()
                                    I_AllesGepeilt:MoveTo(Player)
                                end)
                        end)
                end)
        end)
end

function I_Kaestchen_Cmd01()
    --Oeffnen
    msgMitNachricht(getMessage(language, 147),
        function()
            Stage08.Active = false
            Wherigo.PlayAudio(S_Uhu)
            msgMitNachricht(getMessage(language, 148),
                function()
                    msgMitNachricht(getMessage(language, 149),
                        function()
                            msgMitNachrichtUndBild(getMessage(language, 150),
                                P_EuleEureka,
                                function()
                                    msgMitNachrichtUndBild(getMessage(language, 151),
                                        P_EuleEureka,
                                        function()
                                            I_Kaestchen.Commands.Cmd01.Enabled = false
                                            I_Eulenraetsel:MoveTo(Player)
                                            Wherigo.GetInput(Q_Eulenraetsel)
                                        end)
                                end)
                        end)
                end)
        end)
end

function I_Klappe_Cmd01()
    --Oeffnen
    I_Klappe:MoveTo(nil)
    I_Hebel:MoveTo(Stage09)
    I_Hebel.Commands.Cmd01.Enabled = true
    I_Hebel.Commands.Cmd02.Enabled = true
end

function I_Leseausweis_Cmd01()
    --Zurueckkehren zur Frage
    Wherigo.GetInput(Q_Leseausweis)
end

function I_Lesezeichen_Cmd01()
    I_Lesezeichen.Commands.Cmd01.Enabled = false
    msgMitNachrichtUndBild(getMessage(language, 152),
        P_PalaisStutterheim,
        function()
            msgMitNachrichtUndBild(getMessage(language, 153),
                P_PalaisStutterheim,
                function()
                    msgMitNachrichtUndBild(getMessage(language, 154),
                        P_PalaisStutterheim,
                        function()
                            Stage03.Active = false
                            Stage04.Active = true
                            V_firstEnter = true
                            T_FindeNaechsteZone.Active = true
                            Fortschritt()
                            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                        end)
                end)
        end)
end

function I_MuldiDool_Cmd01()
    --Benutze mit
    T_MuldiDoolVerwenden.Active = false
    Wherigo.GetInput(Q_MuldiDool)
end

function I_RoteLuftballons_Cmd01(target)
    if target == I_Stecknadel then
        msgMitNachricht(getMessage(language, 155),
            function()
                PokeBallon()
            end)
    elseif target == I_MuldiDool then
        msgMitNachricht(getMessage(language, 157),
            function()
                PokeBallon()
            end)
    elseif target == I_Geschichtsbuch then
        msgMitNachricht(getMessage(language, 158),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_RoteLuftballons)
            end)
    end
end

function PokeBallon()
    Wherigo.PlayAudio(S_FarbeBefreit)
    msgMitNachrichtUndBild(getMessage(language, 156),
        P_FarbeBefreitRot,
        function()
            I_RoteLuftballons.Commands.Cmd01.Enabled = false
            V_FarbenBefreit = V_FarbenBefreit + 1
            I_RoteLuftballons:MoveTo(nil)
            I_Stecknadel:MoveTo(nil)
            I_MuldiDool.Media = P_MuldiDoolFA
            I_Tuer:MoveTo(Stage07)
        end)
end

function I_Startfrage_Cmd01()
    Wherigo.GetInput(Q_Startabfrage)
end

function I_Leseausweisfrage_Cmd01()
    Wherigo.GetInput(Q_Leseausweis)
end

function I_TierHugenottenkirche_Cmd01()
    Wherigo.GetInput(Q_TierHugenottenkirche)
end

function I_Beichte_Cmd01()
    Wherigo.GetInput(Q_Beichte)
end

function I_Tuerraetsel_Cmd01()
    Wherigo.GetInput(Q_Tuer)
end

function I_VaterUnser_Cmd01()
    Wherigo.GetInput(Q_VaterUnser)
end

function I_Eulenraetsel_Cmd01()
    Wherigo.GetInput(Q_Eulenraetsel)
end

function I_Raetselcode_Cmd01()
    Wherigo.GetInput(Q_Buchcode)
end

function I_FinalGefunden_Cmd01()
    Wherigo.GetInput(Q_FinalGefunden)
end

function I_RebusAusDose_Cmd01()
    Wherigo.GetInput(Q_RebusAusDose)
end

function I_Stecknadel_Cmd01()
    I_Stecknadel.Image = P_StecknadelSW
    I_Stecknadel:MoveTo(Player)
    I_Stecknadel.Commands.Cmd01.Enabled = false
    msgMitNachrichtUndBild(getMessage(language, 159),
        P_Apothekergasse,
        function()
            T_FindeNaechsteZone.Active = true
            Stage05.Active = false
            Stage06.Active = true
            V_firstEnter = true
            Fortschritt()
            Cd_Speedstrecke56:Start()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function I_Stolperfalle_Cmd01()
    T_MuldiDoolVerwenden.Active = true
    I_MuldiDool.Commands.Cmd01.Enabled = true
    I_Stolperfalle.Commands.Cmd01.Enabled = true
    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
end

function I_Tuer_Cmd01()
    if (V_TuerklopfenAnzahl == 0) then
        msgMitNachricht(getMessage(language, 160),
            function()
                V_TuerklopfenAnzahl = V_TuerklopfenAnzahl + 1
                I_Tuer.Commands.Cmd01.Name = getMessage(language, 161)
            end)
    elseif (V_TuerklopfenAnzahl == 1) then
        I_Tuerraetsel:MoveTo(Player)
        Wherigo.GetInput(Q_Tuer)
    else
        msgMitNachricht("Fehler!", nil)
    end
end

function I_Zettel_Cmd01()
    I_Tuer:MoveTo(Stage07)
    I_Zettel.Commands.Cmd01.Enabled = false
    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
end

function I_ZettelNass_Cmd01()
    I_Raetselcode:MoveTo(Player)
    I_ZettelNass.Icon = P_Icon_RueckertbrunnenZahlen
    I_ZettelNass.Image = P_RueckertbrunnenZahlen
    I_ZettelNass.Name = getMessage(language, 162)
    Wherigo.GetInput(Q_ZettelBrunnen)
end

function I_ZettelNass_Cmd02()
    local d, b = Wherigo.VectorToPoint(Player.ObjectLocation, Stage11.OriginalPoint)
    local distance_rounded = round(d:GetValue('m'), 2)
    I_ZettelNass.Description = getMessage(language, 163) .. distance_rounded .. getMessage(language, 164)
end
