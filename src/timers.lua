function Cd_BrunnenWarten_OnElapsed()
    msgMitNachricht(getMessage(language, 40),
        function()
            Stage10.Active = true
            V_firstEnter = true
            I_Flyer:MoveTo(nil)
            T_FindeNaechsteZone.Active = true
            Fortschritt()
        end)
end

function Cd_RueckertWarten_OnElapsed()
    I_ZettelNass.Description = ""
    I_ZettelNass.Commands.Cmd01.Enabled = true
    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_ZettelNass)
end

function Cd_Speedstrecke56_OnElapsed()
    msgMitNachricht(getMessage(language, 103),
        function()
            Stage06.Active = false
            Stage07.Active = true
            Fortschritt()
        end)
end